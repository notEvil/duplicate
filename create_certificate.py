from shell import *
import pathlib
import sys

_, host = sys.argv

key_path = pathlib.Path(".", host + ".key")
certificate_path = pathlib.Path(".", host + ".pem")

(
    [
        "sudo",
        "openssl",
        "genpkey",
        "-algorithm",
        "EC",
        "-pkeyopt",
        "ec_paramgen_curve:P-256",
        "-out",
        str(key_path),
    ]
    >> shell
    >> wait
)
(
    [
        "sudo",
        "openssl",
        "req",
        "-key",
        str(key_path),
        "-x509",
        "-new",
        "-addext",
        "subjectAltName = DNS:" + host,
        "-days",
        "365",
        "-out",
        str(certificate_path),
        "-batch",
    ]
    >> shell
    >> wait
)
