from shell import *
import pathlib
import sys

_PATH = pathlib.Path(__file__).parent

path = pathlib.Path(_PATH, "python", "vendor", "rpyc")
path.parent.mkdir(parents=True, exist_ok=True)
if not path.exists():
    (
        [
            "git",
            "clone",
            "-b",
            "5.1.0",
            "--depth",
            "1",
            "https://github.com/tomerfiliba-org/rpyc",
            str(path),
        ]
        >> shell
        >> wait
    )
    _ = pathlib.Path(_PATH, "rpyc.patch")
    ["patch", "-p1"] >> shell(cwd=path, stdin=open(_)) >> wait
[sys.executable, "-m", "build"] >> shell(cwd=path) >> wait
