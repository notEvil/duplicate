import numpy
import pymc
import pymc.math as p_math
import pymc.sampling_jax as p_sampling_jax
import math
import os
import sys
import threading


_LESS = -1
_EQUAL = 0
_GREATER = 1


def _get_next(scores, comparisons):
    items = sorted(scores.items(), key=lambda item: item[1])
    min_difference = math.inf
    min_objects = None

    for index, (object, score) in enumerate(items):
        for previous_index in reversed(range(0, index)):
            previous_object, previous_score = items[previous_index]
            if not (
                (object, previous_object) in comparisons
                or (previous_object, object) in comparisons
            ):
                previous_difference = abs(previous_score - score)
                break
        else:
            previous_object = None
            previous_difference = math.inf

        for next_index in range(index + 1, len(items)):
            next_object, next_score = items[next_index]
            if not (
                (object, next_object) in comparisons
                or (next_object, object) in comparisons
            ):
                next_difference = abs(next_score - score)
                break
        else:
            next_object = None
            next_difference = math.inf

        if next_difference < previous_difference:
            second_object = next_object
            difference = next_difference
        else:
            second_object = previous_object
            difference = previous_difference

        if second_object is None:
            continue

        if difference < min_difference:
            min_difference = difference
            min_objects = (object, second_object, difference)

    return min_objects


class _ScoreThread:
    def __init__(self, objects, comparisons):
        super().__init__()

        self.objects = objects
        self.comparisons = comparisons

        self._previous_length = 0
        self._posterior = None
        self._scores = {object: 0 for object in objects}
        self._update_condition = threading.Condition()

        self._thread = threading.Thread(target=self._main)
        self._stop = False
        self._stop_condition = threading.Condition()

        self._thread.start()

    def _main(self):
        p_sampling_jax.sys = _Sys()

        while True:
            if len(self.comparisons) == self._previous_length:
                with self._stop_condition:
                    if self._stop:
                        break

                    self._stop_condition.wait(10)

                continue

            with pymc.Model(coords=dict(objects=self.objects)):
                scores = pymc.Uniform("s", dims="objects")
                scale_1 = pymc.HalfNormal("scale_1")
                scale_2 = pymc.HalfNormal("scale_2")

                left_indices = []
                right_indices = []
                observed = []
                _map = {_LESS: 0, _EQUAL: 1, _GREATER: 2}
                _ = self.comparisons.copy().items()
                for (left_object, right_object), comparison in _:
                    left_indices.append(self.objects.index(left_object))
                    right_indices.append(self.objects.index(right_object))
                    observed.append(_map[comparison])

                d = (scores[left_indices] - scores[right_indices]) * scale_1
                t1 = 1 / (1 + p_math.exp(d))
                t3 = 1 - t1
                t2 = t1 * t3 * scale_2
                _ = t1 + t2 + t3
                p = p_math.stack([t1 / _, t2 / _, t3 / _], 1)

                pymc.Categorical("y", p, observed=observed)

                self._posterior = p_sampling_jax.sample_blackjax_nuts(
                    1000, idata_kwargs=dict(log_likelihood=False)
                ).posterior

            s = self._posterior.s.values.reshape((-1, len(self.objects)))
            for index, object in enumerate(self.objects):
                self._scores[object] = numpy.mean(s[:, index])

            with self._update_condition:
                self._update_condition.notify_all()

            self._previous_length = len(observed)

    def stop(self):
        with self._stop_condition:
            self._stop = True
            self._stop_condition.notify_all()

        self._thread.join()

    def get_sparse_scores(self):
        s = self._posterior.s.values.reshape((-1, len(self.objects)))
        scale_1 = self._posterior.scale_1.values.reshape((-1,))
        scale_2 = self._posterior.scale_2.values.reshape((-1,))

        indices = numpy.argsort(numpy.mean(s, axis=0))
        score = 0
        scores = [score]

        _ = iter(indices)
        previous_index = next(_)
        for index in _:
            p1, p2, p3 = self._get_p(
                s[:, previous_index], s[:, index], scale_1, scale_2
            )
            n1 = numpy.sum((p2 < p1) & (p3 < p1))
            n2 = numpy.sum((p1 < p2) & (p3 < p2))
            if n1 < n2:
                scores.append(score)
            else:
                score += 1
                scores.append(score)

            previous_index = index

        return {self.objects[index]: score for index, score in zip(indices, scores)}

    def _get_p(self, left_score, right_score, scale_1, scale_2):
        d = (left_score - right_score) * scale_1

        t1 = 1 / (1 + numpy.exp(d))
        t3 = 1 - t1
        t2 = t1 * t3 * scale_2
        _ = t1 + t2 + t3
        return (t1 / _, t2 / _, t3 / _)


class _Sys:
    def __init__(self):
        super().__init__()

        self._null_file = open(os.devnull, "wt")

    def __getattr__(self, name):
        if name == "stdout":
            return self._null_file
        return getattr(sys, name)


if __name__ == "__main__":
    import argparse
    import collections
    import pathlib
    import pickle

    parser = argparse.ArgumentParser()
    parser.add_argument("--path", default=None)
    arguments = parser.parse_args()

    if arguments.path is not None:
        arguments.path = pathlib.Path(arguments.path)

    if arguments.path is None or not arguments.path.exists():
        print("Enter strings, 1/line, end with empty line:")

        strings = []

        for line in sys.stdin:
            string = line.strip()
            if string == "":
                break

            strings.append(string)

        comparisons = collections.OrderedDict()
        score_thread = _ScoreThread(strings, comparisons)

    else:
        with open(arguments.path, "rb") as file:
            strings, comparisons = pickle.load(file)

        score_thread = _ScoreThread(strings, comparisons)

        print("waiting for scores")
        with score_thread._update_condition:
            score_thread._update_condition.wait()

    while True:
        object = _get_next(score_thread._scores, comparisons)
        if object is None:
            break

        first_string, second_string, difference = object

        print()
        print(first_string, "|1<2>3|", second_string, "||", "difference:", difference)
        while True:
            string = input("[123uq]: ")
            if len(string) != 1:
                continue

            index = "123uq".find(string)
            if index != -1:
                break

        if index == 3:  # u
            if len(comparisons) == 0:
                continue

            key, comparison = comparisons.popitem()
            while True:
                _ = {_LESS: "<", _EQUAL: "=", _GREATER: ">"}[comparison]
                _ = "undo {} {} {} [yn]? ".format(first_string, _, second_string)
                decision = input(_)
                if len(decision) == 1 and decision in "yn":
                    break

            if decision == "n":
                comparisons[key] = comparison

            continue

        if index == 4:  # q
            break

        _ = {0: _GREATER, 1: _EQUAL, 2: _LESS}[index]
        comparisons[(first_string, second_string)] = _

    if arguments.path is not None:
        with open(arguments.path, "wb") as file:
            pickle.dump((strings, comparisons), file)

    score_thread.stop()

    print()
    _ = score_thread.get_sparse_scores()
    for string, score in sorted(_.items(), key=lambda item: item[1]):
        print(string, ":", score)
