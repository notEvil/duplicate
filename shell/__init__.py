import shlex
import shutil
import signal
import subprocess
import sys
import threading


__all__ = ("DEVNULL", "shell", "wait", "stdout", "background", "interrupt")


DEVNULL = subprocess.DEVNULL


class _Pipe:
    def __init__(self, function, args=None, kwargs=None):
        super().__init__()

        self.function = function
        self.args = args
        self.kwargs = kwargs

    def __call__(self, *args, **kwargs):
        if self.args is not None:
            args = args + self.args
        if self.kwargs is not None:
            kwargs = self.kwargs | kwargs
        return _Pipe(self.function, args=args, kwargs=kwargs)

    def __rrshift__(self, object):
        return self.function(
            object,
            *(() if self.args is None else self.args),
            **({} if self.kwargs is None else self.kwargs)
        )


@_Pipe
def shell(arguments, **kwargs):
    kwargs = dict(stdout=subprocess.PIPE) | kwargs
    print("shell>", shlex.join(arguments), file=sys.stderr)
    return subprocess.Popen(arguments, **kwargs)


@_Pipe
def wait(process, expect=(0,)):
    return_code = process.wait()
    if expect is not None:
        assert return_code in expect


@_Pipe
def stdout(process, file=None):
    if file is None:
        bytes = process.stdout.read()
        wait(process)
        return bytes.decode().strip()

    threading.Thread(target=shutil.copyfileobj, args=(process.stdout, file)).start()
    process.stdout = None
    return process


@_Pipe
def background(process):
    if process.stdout is not None:
        threading.Thread(target=_consume_stdout, args=(process.stdout,)).start()
        process.stdout = None
    return process


def _consume_stdout(stdout):
    with open(os.devnull, "wb") as file:
        shutil.copyfileobj(stdout, file)


@_Pipe
def interrupt(process):
    process.send_signal(signal.SIGINT)
    return process
