package main

/*
#include <stdlib.h> // free
#include <stdint.h> // uintptr_t
*/
import "C"
import (
  badger "github.com/dgraph-io/badger/v3"
  "bytes"
  "runtime/cgo"
  "unsafe"
)


//export free_
func free_(handle_id C.uintptr_t) {
  handle := cgo.Handle(handle_id)
  C.free(handle.Value().(unsafe.Pointer))
  handle.Delete()
}


type IteratorTuple struct {
  iterator *badger.Iterator
  prefix []byte
  prefetch bool
}


var ERROR_CODES = map[error]C.int {
  badger.ErrValueLogSize: 1,
  badger.ErrKeyNotFound: 2,
  badger.ErrTxnTooBig: 3,
  badger.ErrConflict: 4,
  badger.ErrReadOnlyTxn: 5,
  badger.ErrDiscardedTxn: 6,
  badger.ErrEmptyKey: 7,
  badger.ErrInvalidKey: 8,
  badger.ErrBannedKey: 9,
  badger.ErrThresholdZero: 10,
  badger.ErrNoRewrite: 11,
  badger.ErrRejected: 12,
  badger.ErrInvalidRequest: 13,
  badger.ErrManagedTxn: 14,
  badger.ErrNamespaceMode: 15,
  badger.ErrInvalidDump: 16,
  badger.ErrZeroBandwidth: 17,
  badger.ErrWindowsNotSupported: 18,
  badger.ErrPlan9NotSupported: 19,
  badger.ErrTruncateNeeded: 20,
  badger.ErrBlockedWrites: 21,
  badger.ErrNilCallback: 22,
  badger.ErrEncryptionKeyMismatch: 23,
  badger.ErrInvalidDataKeyID: 24,
  badger.ErrInvalidEncryptionKey: 25,
  badger.ErrGCInMemoryMode: 26,
  badger.ErrDBClosed: 27,
}

func _raising(error error, e *C.int) bool {
  if error == nil {
    return false
  }
  *e = ERROR_CODES[error]
  return true
}


//export open
func open(path *C.char, path_length C.int, e *C.int) C.uintptr_t {
  database, error := badger.Open(badger.DefaultOptions(C.GoStringN(path, path_length)))
  if _raising(error, e) {
    return 0
  }
  return C.uintptr_t(cgo.NewHandle(database))
}


//export close
func close(handle_id C.uintptr_t, e *C.int) {
  handle := cgo.Handle(handle_id)
  if _raising(handle.Value().(*badger.DB).Close(), e) {
    return
  }
  handle.Delete()
}


//export new_transaction
func new_transaction(database_handle_id C.uintptr_t, write bool) C.uintptr_t {
  transaction := cgo.Handle(database_handle_id).Value().(*badger.DB).NewTransaction(write)
  return C.uintptr_t(cgo.NewHandle(transaction))
}


//export commit_transaction
func commit_transaction(handle_id C.uintptr_t, e *C.int) {
  handle := cgo.Handle(handle_id)
  if _raising(handle.Value().(*badger.Txn).Commit(), e) {
    return
  }
  handle.Delete()
}


//export discard_transaction
func discard_transaction(handle_id C.uintptr_t) {
  handle := cgo.Handle(handle_id)
  handle.Value().(*badger.Txn).Discard()
  handle.Delete()
}


//export set
func set(transaction_handle_id C.uintptr_t, key_pointer *C.char, key_length C.int, value_pointer *C.char, value_length C.int, e *C.int) {
  _set(cgo.Handle(transaction_handle_id).Value().(*badger.Txn), key_pointer, key_length, value_pointer, value_length, e)
}


//export set_one
func set_one(database_handle_id C.uintptr_t, key_pointer *C.char, key_length C.int, value_pointer *C.char, value_length C.int, e *C.int) {
  transaction := cgo.Handle(database_handle_id).Value().(*badger.DB).NewTransaction(true)
  defer transaction.Discard()

  if _set(transaction, key_pointer, key_length, value_pointer, value_length, e) {
    return
  }
  _raising(transaction.Commit(), e)
}


func _set(transaction *badger.Txn, key_pointer *C.char, key_length C.int, value_pointer *C.char, value_length C.int, e *C.int) bool {
  return _raising(transaction.Set(
    C.GoBytes(unsafe.Pointer(key_pointer), key_length),
    C.GoBytes(unsafe.Pointer(value_pointer), value_length),
  ), e)
}


//export get
func get(transaction_handle_id C.uintptr_t, bytes *C.char, length C.int, result_length *C.int, result_handle *C.uintptr_t, e *C.int) *C.char {
  return _get(cgo.Handle(transaction_handle_id).Value().(*badger.Txn), bytes, length, result_length, result_handle, e)
}


//export get_one
func get_one(database_handle_id C.uintptr_t, bytes *C.char, length C.int, result_length *C.int, result_handle *C.uintptr_t, e *C.int) *C.char {
  transaction := cgo.Handle(database_handle_id).Value().(*badger.DB).NewTransaction(false)
  defer transaction.Discard()

  return _get(transaction, bytes, length, result_length, result_handle, e)
}


func _get(transaction *badger.Txn, pointer *C.char, length C.int, result_length *C.int, result_handle *C.uintptr_t, e *C.int) *C.char {
  item, error := transaction.Get(C.GoBytes(unsafe.Pointer(pointer), length))
  if error != nil {
    if error == badger.ErrKeyNotFound {
      return nil
    }
    _raising(error, e)
    return nil
  }

  bytes, error := item.ValueCopy(nil)
  if _raising(error, e) {
    return nil
  }

  *result_length = C.int(len(bytes))
  
  unsafe_pointer := C.CBytes(bytes)
  *result_handle = C.uintptr_t(cgo.NewHandle(unsafe_pointer))
  return (*C.char)(unsafe_pointer)
}


//export delete_
func delete_(transaction_handle_id C.uintptr_t, bytes *C.char, length C.int, e *C.int) {
  _delete(cgo.Handle(transaction_handle_id).Value().(*badger.Txn), bytes, length, e)
}


//export delete_one
func delete_one(database_handle_id C.uintptr_t, bytes *C.char, length C.int, e *C.int) {
  transaction := cgo.Handle(database_handle_id).Value().(*badger.DB).NewTransaction(true)
  defer transaction.Discard()

  if _delete(transaction, bytes, length, e) {
    return
  }
  _raising(transaction.Commit(), e)
}


func _delete(transaction *badger.Txn, bytes *C.char, length C.int, e *C.int) bool {
  return _raising(transaction.Delete(C.GoBytes(unsafe.Pointer(bytes), length)), e)
}


//export new_iterator
func new_iterator(transaction_handle_id C.uintptr_t, reverse bool, prefix_pointer *C.char, prefix_length C.int, start_pointer *C.char, start_length C.int, prefetch_values bool) C.uintptr_t {
  options := badger.DefaultIteratorOptions
  options.Reverse = reverse
  options.PrefetchValues = prefetch_values
  iterator := cgo.Handle(transaction_handle_id).Value().(*badger.Txn).NewIterator(options)

  var prefix []byte
  if prefix_length == 0 {
    prefix = nil
  } else {
    prefix = C.GoBytes(unsafe.Pointer(prefix_pointer), prefix_length)
  }

  if start_length == 0 {
    if prefix_length == 0 {
      iterator.Rewind()
    } else {
      seek(prefix, reverse, iterator)
    }
  } else {
    iterator.Seek(C.GoBytes(unsafe.Pointer(start_pointer), start_length))
  }

  iterator_tuple := &IteratorTuple{iterator: iterator, prefix: prefix, prefetch: prefetch_values}
  return C.uintptr_t(cgo.NewHandle(iterator_tuple))
}


func seek(bytes_ []byte, reverse bool, iterator *badger.Iterator) {
  if reverse {
    // increment bytes_
    end := make([]byte, len(bytes_))
    copy(end, bytes_)
    if end[len(end) - 1] == 0xff {
      end = append(end, 0)
    } else {
      end[len(end) - 1] += 1
    }
    iterator.Seek(end)
    if iterator.Valid() && bytes.Equal(iterator.Item().Key(), end) {
      iterator.Next()
    }
  } else {
    iterator.Seek(bytes_)
  }
}


//export next
func next(handle_id C.uintptr_t, previous_length int, max_length int, min_bytes int, keys_pointer **C.char, key_lengths_pointer *C.int, key_handles_pointer *C.uintptr_t, values_pointer **C.char, value_lengths_pointer *C.int, value_handles_pointer *C.uintptr_t, e *C.int) int {
  tuple := cgo.Handle(handle_id).Value().(*IteratorTuple)
  keys := unsafe.Slice(keys_pointer, max_length)
  key_lengths := unsafe.Slice(key_lengths_pointer, max_length)
  key_handles := unsafe.Slice(key_handles_pointer, max_length)
  values := unsafe.Slice(values_pointer, max_length)
  value_lengths := unsafe.Slice(value_lengths_pointer, max_length)
  value_handles := unsafe.Slice(value_handles_pointer, max_length)

  if previous_length != 0 {
    for index := 0; index < previous_length; index++ {
      free_(key_handles[index])
      if tuple.prefetch {
        free_(value_handles[index])
      }
    }
  }

  var unsafe_pointer unsafe.Pointer
  total_bytes := 0

  for index := 0; index < max_length; index++ {
    if tuple.prefix == nil {
      if !tuple.iterator.Valid() {
        return index
      }
    } else {
      if !tuple.iterator.ValidForPrefix(tuple.prefix) {
        return index
      }
    }

    if min_bytes <= total_bytes {
      return index
    }

    item := tuple.iterator.Item()

    if tuple.prefetch {
      value_bytes, error := item.ValueCopy(nil)
      if _raising(error, e) {
        return 0
      }
      unsafe_pointer = C.CBytes(value_bytes)
      values[index] = (*C.char)(unsafe_pointer)
      value_lengths[index] = C.int(len(value_bytes))
      value_handles[index] = C.uintptr_t(cgo.NewHandle(unsafe_pointer))

      total_bytes += len(value_bytes)
    }

    key_bytes := item.Key()
    unsafe_pointer = C.CBytes(key_bytes)
    keys[index] = (*C.char)(unsafe_pointer)
    key_lengths[index] = C.int(len(key_bytes))
    key_handles[index] = C.uintptr_t(cgo.NewHandle(unsafe_pointer))

    total_bytes += len(key_bytes)

    tuple.iterator.Next()
  }

  return max_length
}


//export close_iterator
func close_iterator(handle_id C.uintptr_t) {
  handle := cgo.Handle(handle_id)
  handle.Value().(*IteratorTuple).iterator.Close()
  handle.Delete()
}


//export get_sequence
func get_sequence(database_handle_id C.uintptr_t, key_pointer *C.char, key_length C.int, bandwidth uint64, e *C.int) C.uintptr_t {
  sequence, error := cgo.Handle(database_handle_id).Value().(*badger.DB).GetSequence(C.GoBytes(unsafe.Pointer(key_pointer), key_length), bandwidth)
  if _raising(error, e) {
    return 0
  }
  return C.uintptr_t(cgo.NewHandle(sequence))
}


//export next_sequence
func next_sequence(handle_id C.uintptr_t, e *C.int) uint64 {
  number, error := cgo.Handle(handle_id).Value().(*badger.Sequence).Next()
  if _raising(error, e) {
    return 0
  }
  return number
}


//export release_sequence
func release_sequence(handle_id C.uintptr_t, e *C.int) {
  handle := cgo.Handle(handle_id)
  if _raising(handle.Value().(*badger.Sequence).Release(), e) {
    return
  }
  handle.Delete()
}


//export transaction_size
func transaction_size(handle_id C.uintptr_t) float64 {
  return cgo.Handle(handle_id).Value().(*badger.Txn).Size()
}


func main() {
}
