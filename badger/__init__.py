import contextlib
import ctypes
import pathlib


DEFAULT_BATCH_SIZE = 100
DEFAULT_MIN_BYTES = int(1e6)


_ = pathlib.Path(pathlib.Path(__file__).parent, "interface.so")
_interface = ctypes.cdll.LoadLibrary(_)


class Error(Exception):
    pass


class ValueLogSizeError(Error):
    pass


class KeyNotFoundError(Error):
    pass


class TxnTooBigError(Error):
    pass


class ConflictError(Error):
    pass


class ReadOnlyTxnError(Error):
    pass


class DiscardedTxnError(Error):
    pass


class EmptyKeyError(Error):
    pass


class InvalidKeyError(Error):
    pass


class BannedKeyError(Error):
    pass


class ThresholdZeroError(Error):
    pass


class NoRewriteError(Error):
    pass


class RejectedError(Error):
    pass


class InvalidRequestError(Error):
    pass


class ManagedTxnError(Error):
    pass


class NamespaceModeError(Error):
    pass


class InvalidDumpError(Error):
    pass


class ZeroBandwidthError(Error):
    pass


class WindowsNotSupportedError(Error):
    pass


class Plan9NotSupportedError(Error):
    pass


class TruncateNeededError(Error):
    pass


class BlockedWritesError(Error):
    pass


class NilCallbackError(Error):
    pass


class EncryptionKeyMismatchError(Error):
    pass


class InvalidDataKeyIDError(Error):
    pass


class InvalidEncryptionKeyError(Error):
    pass


class GCInMemoryModeError(Error):
    pass


class DBClosedError(Error):
    pass


_ERRORS = {
    0: Error("Unknown error"),
    1: ValueLogSizeError("Invalid ValueLogFileSize), must be in range [1MB), 2GB)"),
    2: KeyNotFoundError("Key not found"),
    3: TxnTooBigError("Txn is too big to fit into one request"),
    4: ConflictError("Transaction Conflict. Please retry"),
    5: ReadOnlyTxnError("No sets or deletes are allowed in a read-only transaction"),
    6: DiscardedTxnError("This transaction has been discarded. Create a new one"),
    7: EmptyKeyError("Key cannot be empty"),
    8: InvalidKeyError("Key is using a reserved !badger! prefix"),
    9: BannedKeyError("Key is using the banned prefix"),
    10: ThresholdZeroError("Value log GC can't run because threshold is set to zero"),
    11: NoRewriteError("Value log GC attempt didn't result in any cleanup"),
    12: RejectedError("Value log GC request rejected"),
    13: InvalidRequestError("Invalid request"),
    14: ManagedTxnError(
        "Invalid API request. Not allowed to perform this action using ManagedDB"
    ),
    15: NamespaceModeError(
        "Invalid API request. Not allowed to perform this action when NamespaceMode is not set."
    ),
    16: InvalidDumpError("Data dump cannot be read"),
    17: ZeroBandwidthError("Bandwidth must be greater than zero"),
    18: WindowsNotSupportedError("Read-only mode is not supported on Windows"),
    19: Plan9NotSupportedError("Read-only mode is not supported on Plan 9"),
    20: TruncateNeededError(
        "Log truncate required to run DB. This might result in data loss"
    ),
    21: BlockedWritesError("Writes are blocked), possibly due to DropAll or Close"),
    22: NilCallbackError("Callback cannot be nil"),
    23: EncryptionKeyMismatchError("Encryption key mismatch"),
    24: InvalidDataKeyIDError("Invalid datakey id"),
    25: InvalidEncryptionKeyError(
        "Encryption key's length should be either 16), 24), or 32 bytes"
    ),
    26: GCInMemoryModeError(
        "Cannot run value log GC when DB is opened in InMemory mode"
    ),
    27: DBClosedError("DB Closed"),
}


class _raising:
    def __init__(self, function):
        super().__init__()

        self.function = function

    def __setattr__(self, name, object):
        if name == "function":
            super().__setattr__(name, object)
            return

        setattr(self.function, name, object)

    def __call__(self, *args):
        error_code = ctypes.c_int(-1)
        result = self.function(*args, ctypes.byref(error_code))
        if error_code.value == -1:
            return result
        raise _ERRORS[error_code.value]


_interface.open = _raising(_interface.open)
_interface.close = _raising(_interface.close)
_interface.commit_transaction = _raising(_interface.commit_transaction)
_interface.set = _raising(_interface.set)
_interface.set_one = _raising(_interface.set_one)
_interface.get = _raising(_interface.get)
_interface.get_one = _raising(_interface.get_one)
_interface.delete_ = _raising(_interface.delete_)
_interface.delete_one = _raising(_interface.delete_one)
_interface.next = _raising(_interface.next)
_interface.get_sequence = _raising(_interface.get_sequence)
_interface.next_sequence = _raising(_interface.next_sequence)
_interface.release_sequence = _raising(_interface.release_sequence)


class _returning_handle:
    def __init__(self, function):
        super().__init__()

        self.function = function

        function.restype = ctypes.c_void_p

    def __setattr__(self, name, object):
        if name == "function":
            super().__setattr__(name, object)
            return

        setattr(self.function, name, object)

    def __call__(self, *args, **kwargs):
        return ctypes.c_void_p(self.function(*args, **kwargs))


_interface.open = _returning_handle(_interface.open)
_interface.new_transaction = _returning_handle(_interface.new_transaction)
_interface.get.restype = ctypes.c_void_p
_interface.get_one.restype = ctypes.c_void_p
_interface.new_iterator = _returning_handle(_interface.new_iterator)
_interface.get_sequence = _returning_handle(_interface.get_sequence)
_interface.transaction_size.restype = ctypes.c_double


@contextlib.contextmanager
def open(path):
    bytes = str(path).encode()
    handle = _interface.open(bytes, len(bytes))
    try:
        yield _Database(handle)
    finally:
        _interface.close(handle)


class _Database:
    def __init__(self, handle):
        super().__init__()

        self.handle = handle

    @contextlib.contextmanager
    def transaction(self, write, always_commit=False):
        transaction = _Transaction(write, self)

        try:
            yield transaction

        except:
            if write and always_commit:
                transaction._commit()
            else:
                transaction._discard()
            raise

        if write:
            transaction._commit()
        else:
            transaction._discard()

    def set(self, key_bytes, value_bytes):
        _interface.set_one(
            self.handle, key_bytes, len(key_bytes), value_bytes, len(value_bytes)
        )

    def get(self, key_bytes):
        length = ctypes.c_int()
        handle = ctypes.c_void_p()
        pointer = _interface.get_one(
            self.handle,
            key_bytes,
            len(key_bytes),
            ctypes.byref(length),
            ctypes.byref(handle),
        )
        if pointer is None:
            return None
        value_bytes = ctypes.string_at(pointer, length)
        _interface.free_(handle)
        return value_bytes

    def delete(self, key_bytes):
        _interface.delete_one(self.handle, key_bytes, len(key_bytes))

    def get_sequence(self, id_bytes, bandwidth):
        return _Sequence(id_bytes, bandwidth, self)


class _Transaction:
    def __init__(self, write, database):
        super().__init__()

        self.write = write
        self.database = database

        self._handle = _interface.new_transaction(database.handle, write)

    def _discard(self):
        _interface.discard_transaction(self._handle)
        self._handle = None

    def _commit(self):
        _interface.commit_transaction(self._handle)
        self._handle = None

    def is_open(self):
        return self._handle is not None

    def set(self, key_bytes, value_bytes):
        _interface.set(
            self._handle, key_bytes, len(key_bytes), value_bytes, len(value_bytes)
        )

    def get(self, key_bytes):
        length = ctypes.c_int()
        handle = ctypes.c_void_p()
        pointer = _interface.get(
            self._handle,
            key_bytes,
            len(key_bytes),
            ctypes.byref(length),
            ctypes.byref(handle),
        )
        if pointer is None:
            return None
        value_bytes = ctypes.string_at(pointer, length)
        _interface.free_(handle)
        return value_bytes

    def delete(self, key_bytes):
        _interface.delete_(self._handle, key_bytes, len(key_bytes))

    def iterator(
        self,
        reverse=False,
        prefix=None,
        start=None,
        prefetch_values=True,
        batch_size=None,
        min_bytes=None,
    ):
        if prefix is not None and start is not None:
            assert start.startswith(prefix)
        if batch_size is None:
            batch_size = DEFAULT_BATCH_SIZE
        if min_bytes is None:
            min_bytes = DEFAULT_MIN_BYTES
        return _Iterator(
            reverse, prefix, start, prefetch_values, batch_size, min_bytes, self
        )

    def get_size(self):
        return _interface.transaction_size(self._handle)


class _Iterator:
    def __init__(
        self,
        reverse,
        prefix,
        start,
        prefetch_values,
        batch_size,
        min_bytes,
        transaction,
    ):
        super().__init__()

        self.reverse = reverse
        self.prefix = prefix
        self.start = start
        self.prefetch_values = prefetch_values
        self.batch_size = batch_size
        self.min_bytes = min_bytes
        self.transaction = transaction

        self._handle = _interface.new_iterator(
            transaction._handle,
            reverse,
            prefix,
            0 if prefix is None else len(prefix),
            start,
            0 if start is None else len(start),
            prefetch_values,
        )
        self._key_pointers = (ctypes.c_void_p * batch_size)()
        self._key_lengths = (ctypes.c_int * batch_size)()
        self._key_handles = (ctypes.c_void_p * batch_size)()
        self._value_pointers = (ctypes.c_void_p * batch_size)()
        self._value_lengths = (ctypes.c_int * batch_size)()
        self._value_handles = (ctypes.c_void_p * batch_size)()
        self._length = 0

    def __enter__(self):
        return self

    def __exit__(self, type, value, traceback):
        self.close()

    def __iter__(self):
        while True:
            if self._handle is None:
                break

            self._length = _interface.next(
                self._handle,
                self._length,
                self.batch_size,
                self.min_bytes,
                ctypes.byref(self._key_pointers),
                ctypes.byref(self._key_lengths),
                ctypes.byref(self._key_handles),
                ctypes.byref(self._value_pointers),
                ctypes.byref(self._value_lengths),
                ctypes.byref(self._value_handles),
            )
            if self._length == 0:
                self.close()
                break

            for _, key_pointer, key_length, value_pointer, value_length in zip(
                range(self._length),
                self._key_pointers,
                self._key_lengths,
                self._value_pointers,
                self._value_lengths,
            ):
                key_bytes = ctypes.string_at(key_pointer, key_length)
                if self.prefetch_values:
                    value_bytes = ctypes.string_at(value_pointer, value_length)
                    yield (key_bytes, value_bytes)
                    continue

                yield key_bytes

    def close(self):
        if self._handle is None:
            return

        for _, key_handle, value_handle in zip(
            range(self._length), self._key_handles, self._value_handles
        ):
            _interface.free_(key_handle)
            if self.prefetch_values:
                _interface.free_(value_handle)

        _interface.close_iterator(self._handle)
        self._handle = None


class _Sequence:
    def __init__(self, id_bytes, bandwidth, database):
        super().__init__()

        self.id_bytes = id_bytes
        self.bandwidth = bandwidth
        self.database = database

        self._handle = _interface.get_sequence(
            database.handle, id_bytes, len(id_bytes), bandwidth
        )

    def __enter__(self):
        return self

    def __exit__(self, type, value, traceback):
        self.close()

    def __iter__(self):
        return self

    def __next__(self):
        if self._handle is None:
            raise StopIteration
        return _interface.next_sequence(self._handle)

    def close(self):
        if self._handle is None:
            return

        _interface.release_sequence(self._handle)
        self._handle = None

    def __del__(self):
        self.close()


class PlyvelDB:
    def __init__(self, path, create_if_missing=False, _interface=None):
        super().__init__()

        self.path = path
        self.create_if_missing = create_if_missing
        self._interface = _interface

        if _interface is None:
            if not create_if_missing and not path.is_dir():
                _ = "Invalid argument: {}: does not exist (create_if_missing is false)"
                raise Exception(_.format(path).encode())

            self._database_context = open(path)
            self._interface = next(self._database_context.gen)

        else:
            self._database_context = None

    def prefixed_db(self, prefix):
        return _PrefixedDB(prefix, self)

    @contextlib.contextmanager
    def transaction(self, write, always_commit=False):
        if self._database_context is None:
            assert not write
            yield self
            return

        _ = self._interface.transaction(write, always_commit=always_commit)
        with _ as transaction:
            yield PlyvelDB(
                self.path,
                create_if_missing=self.create_if_missing,
                _interface=transaction,
            )

    def get_transaction_size(self):
        assert self._database_context is None
        return self._interface.get_size()

    def in_transaction(self):
        return self._database_context is None

    def is_transaction_open(self):
        assert self._database_context is None
        return self._interface.is_open()

    def put(self, key_bytes, value_bytes):
        self._interface.set(key_bytes, value_bytes)

    def get(self, key_bytes, default=None):
        value_bytes = self._interface.get(key_bytes)
        return default if value_bytes is None else value_bytes

    def delete(self, key_bytes):
        self._interface.delete(key_bytes)

    @contextlib.contextmanager
    def iterator(
        self,
        reverse=False,
        start=None,
        include_value=True,
        batch_size=None,
        min_bytes=None,
    ):
        if self._database_context is None:
            with self._interface.iterator(
                reverse=reverse,
                start=start,
                prefetch_values=include_value,
                batch_size=batch_size,
                min_bytes=min_bytes,
            ) as iterator:
                yield iterator

        else:
            with self._interface.transaction(False) as transaction:
                with transaction.iterator(
                    reverse=reverse,
                    start=start,
                    prefetch_values=include_value,
                    batch_size=batch_size,
                    min_bytes=min_bytes,
                ) as iterator:
                    yield iterator

    def get_sequence(self, id_bytes, bandwidth):
        _ = (
            self._interface.database
            if self._database_context is None
            else self._interface
        )
        return _.get_sequence(id_bytes, bandwidth)

    def close(self):
        if self._database_context is None:
            return

        try:
            next(self._database_context.gen)
        except StopIteration:
            self._database_context = None
        else:
            raise Exception


class _PrefixedDB:
    def __init__(self, prefix, plyvel_db):
        super().__init__()

        self.prefix = prefix
        self.plyvel_db = plyvel_db

    def prefixed_db(self, prefix):
        return _PrefixedDB(self.prefix + prefix, self.plyvel_db)

    @contextlib.contextmanager
    def transaction(self, write, always_commit=False):
        _ = self.plyvel_db.transaction(write, always_commit=always_commit)
        with _ as plyvel_db:
            yield _PrefixedDB(self.prefix, plyvel_db)

    def get_transaction_size(self):
        return self.plyvel_db.get_transaction_size()

    def in_transaction(self):
        return self.plyvel_db._database_context is None

    def is_transaction_open(self):
        return self.plyvel_db.is_transaction_open()

    def put(self, key_bytes, value_bytes):
        self.plyvel_db._interface.set(self.prefix + key_bytes, value_bytes)

    def get(self, key_bytes, default=None):
        value_bytes = self.plyvel_db._interface.get(self.prefix + key_bytes)
        return default if value_bytes is None else value_bytes

    def delete(self, key_bytes):
        self.plyvel_db._interface.delete(self.prefix + key_bytes)

    @contextlib.contextmanager
    def iterator(
        self,
        reverse=False,
        start=None,
        include_value=True,
        batch_size=None,
        min_bytes=None,
    ):
        length = len(self.prefix)
        if self.plyvel_db._database_context is None:
            with self.plyvel_db._interface.iterator(
                reverse=reverse,
                prefix=self.prefix,
                start=None if start is None else (self.prefix + start),
                prefetch_values=include_value,
                batch_size=batch_size,
                min_bytes=min_bytes,
            ) as iterator:
                if include_value:
                    yield (
                        (key_bytes[length:], value_bytes)
                        for key_bytes, value_bytes in iterator
                    )

                else:
                    yield (key_bytes[length:] for key_bytes in iterator)

        else:
            with self.plyvel_db._interface.transaction(False) as transaction:
                with transaction.iterator(
                    reverse=reverse,
                    prefix=self.prefix,
                    start=None if start is None else (self.prefix + start),
                    prefetch_values=include_value,
                    batch_size=batch_size,
                    min_bytes=min_bytes,
                ) as iterator:
                    if include_value:
                        yield (
                            (key_bytes[length:], value_bytes)
                            for key_bytes, value_bytes in iterator
                        )

                    else:
                        yield (key_bytes[length:] for key_bytes in iterator)

    def get_sequence(self, id_bytes, bandwidth):
        return self.plyvel_db.get_sequence(self.prefix + id_bytes, bandwidth)
