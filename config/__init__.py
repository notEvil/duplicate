import pydantic
import pathlib
import typing


class _EmptyNone(pathlib.Path):
    @classmethod
    def __get_validators__(cls):
        def validate(object):
            if object == "":
                return None
            raise ValueError

        return [validate]


Path = typing.Union[_EmptyNone, pathlib.Path]


class Configuration(pydantic.BaseModel):
    drive_id: int
    root_path: Path
    drives: dict[int, tuple[str, int]]
    ssl_key: Path
    ssl_certificate: Path
    ssh_private: Path
    ssh_public: Path
    mime_ranks: dict[str, int]
    port: int
    blacklist: list[Path] = [
        pathlib.Path("bin"),
        pathlib.Path("boot"),
        pathlib.Path("lib"),
        pathlib.Path("lib32"),
        pathlib.Path("lib64"),
        pathlib.Path("opt"),
        pathlib.Path("sbin"),
        pathlib.Path("usr"),
        pathlib.Path("var", "cache"),
        pathlib.Path("var", "lib", "apt"),
        pathlib.Path("var", "lib", "archbuild"),
        pathlib.Path("var", "lib", "dkms"),
        pathlib.Path("var", "lib", "dpkg"),
        pathlib.Path("var", "lib", "flatpak"),
        pathlib.Path("var", "lib", "pacman"),
        pathlib.Path("var", "lock"),
        pathlib.Path("var", "tmp"),
        pathlib.Path("var", "run"),
    ]
    user_blacklist: list[Path] = [
        pathlib.Path(".Mathematica"),
        pathlib.Path(".PlayOnLinux"),
        pathlib.Path(".buildozer"),
        pathlib.Path(".cache"),
        pathlib.Path(".cargo"),
        pathlib.Path(".config", "discord", "Cache"),
        pathlib.Path(".config", "discord", "Code Cache"),
        pathlib.Path(".config", "skypeforlinux", "Cache"),
        pathlib.Path(".dropbox-dist"),
        pathlib.Path(".eclipse"),
        pathlib.Path(".gradle", "caches"),
        pathlib.Path(".gradle", "wrapper", "dists"),
        pathlib.Path(".local"),
        pathlib.Path(".m2", "repository"),
        pathlib.Path(".minecraft", "assets"),
        pathlib.Path(".npm", "_cacache"),
        pathlib.Path(".vpython-root"),
        pathlib.Path(".wine"),
        pathlib.Path("R"),
        pathlib.Path("go"),
    ]
    name_blacklist: list[str] = [".venv", "node_modules"]
    log_level: str = "INFO"
    user_name: str = "duplicate"
    authorized_keys: Path = None
    pacman_local: Path = pathlib.Path("var", "lib", "pacman", "local")
    pacman_cache: Path = pathlib.Path("var", "cache", "pacman", "pkg")
