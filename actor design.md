## Context

### Persistent storage

-   entry db

    -   stores information about files on disk

    -   shared between distributor and custodian

    -   entry

        -   path

        -   stat

        -   hash

        -   “score”

            -   mime

            -   compressed size

-   duplicate db

    -   stores information about existence of files

        -   distributor: existence of duplicates

        -   custodian: existence of originals

    -   duplicate

        -   hash

        -   drive ids

### Temporary storage

-   score db

    -   stores mapping of files to scores ordered by score

    -   row

        -   entry id

        -   score

## Actors

### distributor

1.  if entry db is out of date

    1.  scan file system and update entry db

        -   ignore placeholder (see custodian)

2.  create score db

    -   ignore files from custodian

    -   ignore files in duplicate db

3.  while entry db is up to date

    1.  get files to distribute

    2.  send duplication request to custodians

        -   \[(hash, score, size)\]

    3.  if no responses or failure

        1.  continue

    4.  for drive id, response in responses

        1.  for (hash, score, size), state in zip(request, response)

            1.  if state is exists

                1.  add (hash, drive id) to duplicate db

    5.  if no custodian accepted any file

        1.  continue

    6.  choose custodian

    7.  send transfer request to custodian

        1.  \[(hash, score, size)\]

    8.  if rejection or no response or failure

        1.  continue

    9.  for each file

        1.  transfer file

        2.  if cancelled or failed

            1.  break

        3.  add (hash, drive id) to duplicate db

### custodian

1.  remove placeholder from entry db

2.  create score db

3.  on duplication request

    1.  check transfers if not already checked within 60s [1]

        1.  for each transfer

            1.  for file in transfer [2]

                1.  if current size is final size

                    1.  update entry db

                    2.  remove file from transfer

            2.  if transfer is empty

                1.  continue

            3.  check transfer heartbeat

            4.  if transfer is dead

                1.  for file in transfer

                    1.  remove placeholder from entry db and score db

                2.  continue

            5.  yield progress

    2.  get free space

        -   disk usage

        -   transfer progress<span id="transfer-progress" label="transfer-progress"></span>

    3.  for hash, score, size in request

        1.  if hash in entry db

            1.  yield (hash, exists)

            2.  continue

        2.  while not enough free space

            1.  get duplicate with least score

            2.  if no duplicate exists

                1.  yield (hash, rejected) from now on

            3.  mark duplicate for deletion [3]

        3.  yield (hash, accepted)

4.  on transfer request

    1.  process like duplication request

    2.  if any rejected

        1.  return reject

    3.  for content in request

        1.  add placeholder to entry db and score db

    4.  for duplicate in marked duplicates [4]

        1.  if duplicate in transfer

            1.  cancel transfer

            2.  <s>check like duplication request</s> [5]

        2.  delete file if exists

        3.  remove file from entry db and score db if exist

    5.  remember transfer

    6.  return accept

## Notes

-   <a href="#transfer-progress" data-reference-type="ref" data-reference="transfer-progress">[transfer-progress]</a>

    -   expensive due to stat calls

    -   if updated only intermittently, the true free space is underestimated

        -   might lead to spurious deletes of duplicates or transfer cancellations

        -   mitigated by <a href="#rejection-window" data-reference-type="ref" data-reference="rejection-window">[rejection-window]</a>

## Features

-   rejection window<span id="rejection-window" label="rejection-window"></span>

    -   after reaching a soft limit regarding free space, the custodian rejects transfers

    -   after reaching a hard limit regarding free space, the custodian deletes duplicates

    -   this window prevents continuous transfers and deletions in case of flicker

## Footnotes

[1] moved to background thread because request is time constrained

[2] moved to end of transfer because final sizes are unknown due to compression

[3] see <a href="#rejection-window" data-reference-type="ref" data-reference="rejection-window">[rejection-window]</a>

[4] see <a href="#rejection-window" data-reference-type="ref" data-reference="rejection-window">[rejection-window]</a>

[5] delegated to background thread
