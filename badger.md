# Badger

*“[BadgerDB](https://github.com/dgraph-io/badger) is an embeddable, persistent and fast key-value (KV) database written in pure Go.”*

This Python package provides a mostly transparent, pythonic interface to *BadgerDB* using ctypes and cgo.

### Reasons to consider

- *BadgerDB* is an excellent key-value database with many features

  - I chose *BadgerDB* for *[duplicate](https://gitlab.com/notEvil/duplicate)* because it tries to minimize write amplification and has a very small disk footprint due to *[Zstandard](https://github.com/facebook/zstd)* compression

- It provides

  - an **improved reverse prefix scan**

    *BadgerDB* doesn’t behave as one would expect, see [Issue 436](https://github.com/dgraph-io/badger/issues/436)

  - the **relative size of transactions**

    *BadgerDB* fails with `ErrTxnTooBig` if the transaction grows too large, see <https://discuss.dgraph.io/t/knowing-the-limit-of-writes-in-a-transaction/2898/8>

  - an **interface** matching [plyvel](https://github.com/wbolster/plyvel)`.DB`

  - Context managers

  - Error propagation

### Reasons to avoid

- There is **no test**-suite (yet)

  It is used extensively in *duplicate* though

- It is **“slow”** due to cgo and Python

  In my (non-universal) tests, Go was ~4x faster at ingest and scan

### Quickstart

- Install

  TODO

- Open database

```python
import badger
import pathlib

with badger.open(pathlib.Path('/path/to/database')) as database:
    pass
```

- Create transaction

```python
with database.transaction(
    write=True,
    always_commit=False,
) as transaction:
    pass
```

- Use transaction

```python
transaction.set(b'key', b'value')
transaction.get(b'key')
transaction.delete(b'key')

with transaction.iterator(
    reverse=False,
    prefix=None,
    start=None,
    prefetch_values=True,
    batch_size=None,  # default: 100
    min_bytes=None,  # default 1_000_000, max(batched bytes) = min_bytes + max(item bytes)
) as iterator:
    for key_bytes, value_bytes in iterator:
        pass

transaction.get_size()
```
- Sequence

```python
with database.get_sequence(
    id_bytes=b'sequence id',
    bandwidth=1,
) as sequence:
    next(sequence)
```
- Plyvel interface

```python
plyvel_db = badger.PlyvelDB(
    path=pathlib.Path('/path/to/database'),
    create_if_missing=True,
)

prefixed_db = plyvel_db.prefixed_db(b'prefix')

plyvel_db.put(b'key', b'value')
plyvel_db.get(b'key')
plyvel_db.delete(b'key')

plyvel_db.close()
```
- Extended Plyvel interface

```python
with plyvel_db.transaction(
    write=True,
    always_commit=False,
) as t_plyvel_db:
    # use object like main object
    t_plyvel_db.get_transaction_size()

with plyvel_db.iterator(
    reverse=False,
    start=None,
    include_value=True,
    batch_size=None,
    min_bytes=None,
) as iterator:
    for key_bytes, value_bytes in iterator:
        pass

with plyvel_db.get_sequence(
    id_bytes=b'sequence id',
    bandwidth=1,
) as sequence:
    pass
```

### TODO

- Quickstart/Install
- Add tests
- Write API documentation
