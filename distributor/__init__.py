import badger
import builtin
import collector
import common
import distributor.arch as d_arch
import rofs
import sap
import git
import magic
import base64
import datetime
import logging
import math
import pathlib
import pickle
import pwd
import stat
import threading
import time
import traceback


SKIP_DELTA = datetime.timedelta(days=1)

COMMIT_SECONDS = 5

_MIN_SIZE = int(10 * 1e6)  # 10MB
_MAX_SIZE = int(11 * 1e6)  # 11MB

_TYPE_MASK = stat.S_IFREG | stat.S_IFDIR | stat.S_IFLNK


def main(configuration, exit_functions):
    _ = configuration.base
    custodians = common.Custodians(_.drives, _.ssl_key, _.ssl_certificate)
    exit_functions.append(custodians.close)

    with common.entry_db(configuration.custodian_path, configuration) as entry_db:
        exit_event = threading.Event()
        _ = (
            entry_db,
            custodians,
            configuration.home_path,
            False,
            configuration.custodian_path,
            configuration,
            exit_event,
        )
        collector_thread = threading.Thread(target=collector.main, args=_)

        def _exit():
            exit_event.set()
            with custodians._condition:
                custodians._condition.notify_all()
            collector_thread.join()

        collector_thread.start()
        exit_functions.append(_exit)

        last_update_path = pathlib.Path(configuration.home_path, "last_update")

        if last_update_path.exists():
            with open(last_update_path, "r") as file:
                last_update = file.read()
            last_update = datetime.datetime.fromisoformat(last_update)

        else:
            last_update = datetime.datetime.min

        while True:
            now = datetime.datetime.now()
            if common.UPDATE_INTERVAL <= (now - last_update):
                _update_entries(entry_db, configuration)

                last_update = now
                with open(last_update_path, "w") as file:
                    file.write(last_update.isoformat())

            _distribute(entry_db, custodians, last_update, configuration)

            logging.info("Waiting for next filesystem scan")
            time.sleep(_get_seconds(last_update))


def _update_entries(entry_db, configuration):
    base_path = pathlib.Path(configuration.rofs_path, "b")

    local_path = configuration.base.pacman_local
    if local_path is not None:
        local_path = pathlib.Path(base_path, local_path)

    cache_path = configuration.base.pacman_cache
    if cache_path is not None:
        cache_path = pathlib.Path(base_path, cache_path)

    logging.info("Preparing filesystem scan")
    arch = d_arch.Arch(
        pathlib.Path(configuration.home_path, "arch"),
        local_path=local_path,
        cache_path=cache_path,
    )

    logging.info("Scanning filesystem")

    magic_ = magic.open(magic.MIME_TYPE)
    assert magic_.load() == 0
    decompress_magic = magic.open(magic.MIME_TYPE | magic.COMPRESS)
    assert decompress_magic.load() == 0
    magics = (magic_, decompress_magic)

    black_paths = {
        pathlib.Path(base_path, path) for path in configuration.base.blacklist
    }

    for home_path in _get_home_paths(configuration):
        _ = configuration.base.user_blacklist
        black_paths.update(pathlib.Path(home_path, path) for path in _)

    add_count = 0
    change_count = 0
    remove_count = 0

    def _scan(start_parts):
        _ = configuration.base.name_blacklist
        for path, stat in _get_paths(base_path, start_parts, black_paths, _, None):
            relative_path = path.relative_to(base_path)

            if arch.is_unmodified(relative_path, stat):
                continue

            yield (path, stat, relative_path)

    start_dictionary = None
    start_parts = []
    start_path = None

    while True:
        try:
            with entry_db._path_dictionary.iterator(start=start_dictionary) as iterator:
                _ = [
                    (_scan(start_parts), lambda objects: common.get_key(objects[2])),
                    (iterator, lambda item: item[0]),
                ]
                iterator = iter(builtin.zip_sorted(_))

                while True:
                    last_dictionary = None
                    last_path = None
                    start_time = time.monotonic()

                    with entry_db.transaction(True) as t_entry_db:
                        for objects, db_item in iterator:
                            if COMMIT_SECONDS <= (time.monotonic() - start_time):
                                break

                            if (
                                db_item is not None and db_item[1]["stat"].st_mode == 0
                            ):  # ignore placeholder
                                continue

                            if objects is None:  # file was removed
                                _, entry_dictionary = db_item
                                last_dictionary = entry_dictionary

                                t_entry_db._files.remove(entry_dictionary)
                                remove_count += 1

                                if t_entry_db.is_full():
                                    break
                                continue

                            path, stat, relative_path = objects
                            last_path = relative_path

                            if db_item is None:  # new file
                                entry_dictionary = dict(path=relative_path)
                                try:
                                    common.update_metadata(path, stat, entry_dictionary)
                                    common.update_content(
                                        path, stat, entry_dictionary, magics
                                    )
                                except FileNotFoundError:
                                    continue

                                t_entry_db._files.add(entry_dictionary)
                                add_count += 1

                                if t_entry_db.is_full():
                                    break
                                continue

                            _, entry_dictionary = db_item
                            last_dictionary = entry_dictionary

                            db_stat = entry_dictionary["stat"]

                            if (
                                stat.st_mtime_ns == db_stat.st_mtime_ns
                                and stat.st_size == db_stat.st_size
                                and stat.st_ctime_ns == db_stat.st_ctime_ns
                                and (stat.st_mode & rofs._MODE_MASK)
                                == (db_stat.st_mode & rofs._MODE_MASK)
                            ):
                                continue

                            try:
                                stat = _get_stat(path, configuration)
                                common.update_metadata(path, stat, entry_dictionary)

                            except FileNotFoundError:
                                t_entry_db._files.remove(entry_dictionary)
                                remove_count += 1

                                if t_entry_db.is_full():
                                    break
                                continue

                            if not (
                                stat.st_mtime_ns == db_stat.st_mtime_ns
                                and stat.st_size == db_stat.st_size
                            ):  # content changed
                                try:
                                    common.update_content(
                                        path, stat, entry_dictionary, magics
                                    )

                                except FileNotFoundError:
                                    t_entry_db._files.remove(entry_dictionary)
                                    remove_count += 1

                                    if t_entry_db.is_full():
                                        break
                                    continue

                            t_entry_db._files.add(entry_dictionary)
                            change_count += 1

                            if t_entry_db.is_full():
                                break

                        else:
                            break

                    _ = "added: {}, changed: {}, removed: {}"
                    logging.debug(_.format(add_count, change_count, remove_count))

                    if last_dictionary is not None:
                        start_dictionary = last_dictionary
                    if last_path is not None:
                        start_path = last_path

        except badger.ConflictError:
            start_parts.clear()
            if start_path is not None:
                start_parts.extend(reversed(start_path.parts))
            continue

        break

    _ = "added: {}, changed: {}, removed: {}"
    logging.debug(_.format(add_count, change_count, remove_count))
    logging.info("done")


def _get_home_paths(configuration):
    base_path = pathlib.Path(configuration.rofs_path, "b")

    path = pathlib.Path(base_path, "etc", "passwd")
    if not path.exists():
        return

    with open(path, "rt") as file:
        for line in file:
            parts = line.strip().split(":")

            path = parts[-2]
            if path == "/":
                continue

            yield pathlib.Path(base_path, pathlib.Path(path[1:]))


def _get_paths(path, start_parts, black_paths, black_names, white_paths):
    try:
        _ = ((builtin.encode(path.name), path) for path in path.iterdir())
        path_items = sorted(_)
    except (FileNotFoundError, NotADirectoryError):
        return

    if len(start_parts) != 0:
        start_key = builtin.encode(start_parts.pop())
        for index, (key, _) in enumerate(path_items):
            if key < start_key:
                continue

            if key == start_key:
                if len(start_parts) == 0:
                    index += 1  # exclusive
                break

            start_parts.clear()
            break

        else:
            return

        path_items = path_items[index:]

    for _, path in path_items:
        if white_paths is not None and not any(
            path.is_relative_to(white_path) or white_path.is_relative_to(path)
            for white_path in white_paths
        ):
            continue

        try:
            stat_ = path.lstat()
        except FileNotFoundError:
            continue

        if (stat_.st_mode & _TYPE_MASK) == 0:
            continue

        yield (path, stat_)

        if (
            (stat_.st_mode & stat.S_IFDIR) == 0
            or path in black_paths
            or path.name in black_names
        ):
            continue

        if pathlib.Path(path, ".git").exists():
            try:
                repository = git.Repo(path)

            except git.InvalidGitRepositoryError:
                next_white_paths = white_paths

            else:
                _ = repository.untracked_files
                next_white_paths = [pathlib.Path(path, string) for string in _]
                _ = repository.index.diff(None)
                next_white_paths.extend(pathlib.Path(path, diff.a_path) for diff in _)

        else:
            next_white_paths = white_paths

        _ = _get_paths(path, start_parts, black_paths, black_names, next_white_paths)
        yield from _


def _get_stat(path, configuration):
    _ = pathlib.Path(configuration.rofs_path, "stat", rofs.get_shortcut_name(path))
    with open(_, "rb") as file:
        return pickle.load(file)


def _distribute(entry_db, custodians, last_update, configuration):
    logging.info("Distributing")

    custodian_path = configuration.custodian_path
    now = datetime.datetime.utcnow().replace(tzinfo=datetime.timezone.utc)
    contents = {}
    start_dictionary = None

    while True:
        if common.UPDATE_INTERVAL <= (datetime.datetime.now() - last_update):
            break

        total_size = sum(size for _, size, _ in contents.values())

        with entry_db.transaction(False) as t_entry_db:
            with t_entry_db._score_dictionary.iterator(
                prefix=False, start=start_dictionary
            ) as iterator:
                for score, entry_dictionary in iterator:
                    hash = entry_dictionary["hash"]
                    if not (
                        SKIP_DELTA <= (now - _get_datetime(entry_dictionary))
                        and not t_entry_db._distributor_duplicates.contains(hash)
                        and not entry_dictionary["path"].is_relative_to(custodian_path)
                        and hash not in contents
                    ):
                        start_dictionary = entry_dictionary
                        continue

                    size = entry_dictionary["size"]
                    uncompressed_size = entry_dictionary["stat"].st_size
                    if (uncompressed_size * common.COMPRESSION_FACTOR) < size:
                        size = uncompressed_size
                        compressed = False
                    else:
                        compressed = True

                    if _MAX_SIZE < (total_size + size) and len(contents) != 0:
                        break

                    contents[hash] = (score, size, compressed)
                    total_size += size

                    start_dictionary = entry_dictionary

                    if _MIN_SIZE <= total_size or len(contents) == common._BATCH_SIZE:
                        break

        if len(contents) == 0:
            logging.info("done")
            break

        objects = _request_duplication(contents, size, custodians, last_update)
        if objects is None:
            break
        tuples, responses = objects

        drive_id, states = _choose_response(tuples, responses, contents, entry_db)
        if len(contents) == 0:  # all exist
            continue

        if drive_id is None:
            logging.debug("Waiting for custodian or next try")
            with custodians._condition:
                # don't give up entirely
                _ = min(common.RETRY_SECONDS, _get_seconds(last_update))
                custodians._condition.wait(_)

            continue

        custodian = custodians.get(drive_id)
        if custodian is None:
            continue

        objects = _request_transfer(tuples, states, custodian, configuration)
        if objects is None:
            continue
        tuples, transfer_response = objects

        _transfer(
            tuples, transfer_response, drive_id, contents, entry_db, configuration
        )
        if len(contents) != 0:
            logging.debug("retry: {}".format(len(contents)))


def _get_datetime(entry_dictionary):
    _ = entry_dictionary["stat"].st_mtime
    return datetime.datetime.fromtimestamp(_, tz=datetime.timezone.utc)


def _request_duplication(contents, size, custodians, last_update):
    logging.debug("Requesting duplication: {:.2f}MB".format(size / 1e6))
    tuples = tuple(contents.items())

    while True:
        with custodians._condition:
            custodian_items = custodians.get_items()

            if len(custodian_items) == 0:
                logging.debug("Waiting for custodians")
                if not custodians._condition.wait(_get_seconds(last_update)):
                    return None
                logging.debug("done")

                custodian_items = custodians.get_items()

        responses = _Responses()

        for _, custodian in custodian_items:
            if not custodian.request_duplication(tuples, responses.add_request()):
                responses.remove_request()

        responses = responses.get(common.TIMEOUT_SECONDS)
        if len(responses) != 0:
            break

        logging.debug("no response")

    return (tuples, responses)


def _get_seconds(last_update):
    _ = (last_update + common.UPDATE_INTERVAL - datetime.datetime.now()).total_seconds()
    return max(0, _)


class _Responses:
    def __init__(self):
        super().__init__()

        self._lock = threading.Lock()
        self._count = 0
        self._event = threading.Event()
        self._responses = []

        self._event.set()

    def add_request(self):
        with self._lock:
            self._count += 1
            self._event.clear()

        return self._add_response

    def remove_request(self):
        with self._lock:
            self._count -= 1
            if self._count == 0:
                self._event.set()

    def _add_response(self, response):
        with self._lock:
            self._responses.append(response)

            self._count -= 1
            if self._count == 0:
                self._event.set()

    def get(self, timeout):
        if self._event.wait(timeout):
            return self._responses
        return self._responses.copy()


def _choose_response(tuples, responses, contents, entry_db):
    min_key = (b"\xff" * 9, math.inf)
    min_drive_id = None
    min_states = None

    for states, drive_id in responses:
        while True:
            iterator = iter(zip(tuples, states))
            response_score = None
            response_count = 0

            try:
                while True:
                    with entry_db.transaction(True) as t_entry_db:
                        for (hash, (score, _, _)), state in iterator:
                            if state == common._EXISTS:
                                t_entry_db._distributor_duplicates.add(hash, drive_id)
                                contents.pop(hash, None)

                                if t_entry_db.is_full():
                                    break

                            elif state == common._ACCEPT:
                                if response_score is None:
                                    response_score = score
                                    response_count = 1

                                elif score == response_score:
                                    response_count += 1

                        else:
                            break

            except badger.ConflictError:
                continue

            break

        if response_score is None:
            continue

        key = (response_score, -response_count)
        if key < min_key:
            min_key = key
            min_drive_id = drive_id
            min_states = states

    return (min_drive_id, min_states)


def _request_transfer(tuples, states, custodian, configuration):
    tuples = tuple(_ for _, state in zip(tuples, states) if state == common._ACCEPT)

    _ = sum(size for _, (_, size, _) in tuples)
    logging.debug("Requesting transfer: {:.2f}MB".format(_ / 1e6))

    transfer_response = _TransferResponse()

    _ = (tuples, configuration.base.drive_id)
    if not custodian.request_transfer(_, transfer_response):
        return None

    with transfer_response._condition:
        if (
            transfer_response._accepted is None
            and not transfer_response._condition.wait(common.TIMEOUT_SECONDS)
        ):
            return None

    return (tuples, transfer_response) if transfer_response._accepted else None


class _TransferResponse:
    def __init__(self):
        super().__init__()

        self._accepted = None
        self._cancelled = False
        self._condition = threading.Condition()
        self._end = False

    def __call__(self, argument):
        if argument is None:  # heartbeat
            return self._end

        with self._condition:
            if self._accepted is None:
                self._accepted = argument
            else:
                self._cancelled = self._cancelled or argument

            self._condition.notify_all()


def _transfer(tuples, transfer_response, drive_id, contents, entry_db, configuration):
    logging.debug("Transfering")

    base_path = pathlib.Path(configuration.rofs_path, "b")

    def _items():
        for hash, (_, _, compressed) in tuples:
            if hash not in contents:
                continue

            from_path = None
            for entry_dictionary in entry_db._hash_dictionary.get(dict(hash=hash)):
                if compressed:
                    try:
                        name = rofs.get_shortcut_name(entry_dictionary["path"])
                    except FileNotFoundError:
                        continue

                    _ = configuration.rofs_path
                    from_path = pathlib.Path(_, "compress_zlib", name)

                else:
                    from_path = pathlib.Path(base_path, entry_dictionary["path"])

                if from_path.exists():
                    break

            if from_path is None:
                del contents[hash]
                continue

            to_path = common.get_duplicate_path(hash)
            if compressed:
                to_path = to_path.with_suffix(".zst")

            yield (hash, (from_path, (drive_id, to_path)))

    for retry_count in range(3):
        if retry_count != 0:
            time.sleep(10)

        if transfer_response._cancelled:
            logging.debug("cancelled")
            return

        try:
            _ = sap.append(
                _items(),
                configuration.base.user_name,
                configuration.base.drives[drive_id][0],
                configuration.base.ssh_private,
                _Cancelled(transfer_response),
            )
            for hash, transferred in _:
                del contents[hash]
                if transferred:
                    entry_db._distributor_duplicates.add_one(hash, drive_id)

        except:  # TODO
            logging.debug("failed: {}".format(traceback.format_exc()))
            continue

        if transfer_response._cancelled:
            logging.debug("cancelled")
            return

        break

    else:
        transfer_response._end = True
        logging.debug("failed")
        return

    transfer_response._end = True
    logging.debug("done")


class _Cancelled:
    def __init__(self, transfer_response):
        super().__init__()

        self.transfer_response = transfer_response

    def __bool__(self):
        return self.transfer_response._cancelled
