import badger
import builtin
import common
import regex
import zstandard
import contextlib
import gzip
import itertools
import pathlib
import pickle
import shutil
import tarfile


class Arch:
    def __init__(self, home_path, local_path=None, cache_path=None, update=True):
        super().__init__()

        self.home_path = home_path
        self.local_path = local_path
        self.cache_path = cache_path
        self.update = update

        self._mtree = _Mtree(home_path, local_path, cache_path, update=update)

    def is_unmodified(self, path, stat):
        return (stat.st_mtime, stat.st_size) in self._mtree.get(path)


class _Mtree:
    def __init__(self, home_path, local_path, cache_path, update=True):
        super().__init__()

        self.home_path = home_path
        self.local_path = local_path
        self.cache_path = cache_path
        self.update = update

        self._path = pathlib.Path(home_path, "mtree")

        if update:
            self._plyvel_db = None
            self._update()

        else:
            self._plyvel_db = badger.PlyvelDB(self._path)

    def _update(self):
        if self._plyvel_db is not None:
            self._plyvel_db.close()
        if self._path.exists():
            shutil.rmtree(self._path)
        self._plyvel_db = badger.PlyvelDB(self._path, create_if_missing=True)

        while True:
            iterators = []
            if self.local_path is not None:
                iterators.append(_get_local_files(self.local_path))
            if self.cache_path is not None:
                iterators.append(_get_cache_files(self.cache_path))

            iterator = iter(itertools.chain(*iterators))

            try:
                while True:
                    with self._plyvel_db.transaction(True) as t_plyvel_db:
                        for path, attributes in iterator:
                            time = attributes.get("time")
                            size = (
                                len(attributes["link"])
                                if attributes.get("type") == "link"
                                else attributes.get("size")
                            )
                            if time is None or size is None:
                                continue

                            key = builtin.encode(path)

                            bytes = t_plyvel_db.get(key)
                            ids = [] if bytes is None else pickle.loads(bytes)

                            id = (float(time), int(size))
                            if id in ids:
                                continue

                            ids.append(id)
                            t_plyvel_db.put(key, pickle.dumps(ids))

                            if (
                                common._TRANSACTION_SIZE
                                <= t_plyvel_db.get_transaction_size()
                            ):
                                break
                        else:
                            break

            except badger.ConflictError:
                continue

            break

    def get(self, path):
        bytes = self._plyvel_db.get(builtin.encode(str(path)))
        if bytes is None:
            return ()
        return pickle.loads(bytes)

    def __del__(self):
        self._plyvel_db.close()


def _get_local_files(path):
    if not path.exists():
        return

    for path in path.iterdir():
        if not path.is_dir():
            continue

        path = pathlib.Path(path, "mtree")
        if not path.exists():
            continue

        with open(path, "rb") as file:
            yield from _get_files(file)


def _get_cache_files(path):
    if not path.exists():
        return

    for path in path.iterdir():
        if path.name.endswith(".sig"):
            continue

        with open(path, "rb") as file:
            with _open_tar(file, path) as tar_file:
                for member in tar_file:
                    if member.name == ".MTREE":
                        break

                else:
                    raise Exception

                yield from _get_files(tar_file.extractfile(member))


@contextlib.contextmanager
def _open_tar(file, path):
    if path.name.endswith(".zst"):
        with zstandard.ZstdDecompressor().stream_reader(file) as reader:
            with tarfile.open(fileobj=reader, mode="r|") as tar_file:
                yield tar_file

    elif path.name.endswith(".xz"):
        with tarfile.open(fileobj=file, mode="r|xz") as tar_file:
            yield tar_file

    else:
        raise NotImplementedError


def _get_files(file):
    with gzip.open(file, "rt") as file:
        for line in file:
            line = line.strip()

            if line == "" or line[0] in "#/":
                continue

            _ = r"^(?P<path>.+?)(\s+(?P<name>[^=]+)=(?P<value>\S+))*$"
            match = regex.search(_, line)
            if match is None:
                assert "time=" not in line  # INFO seems to be the case
                continue

            path = match.group("path")
            assert path.startswith("./")
            path = path[len("./") :]

            _ = dict(zip(match.captures("name"), match.captures("value")))
            yield (path, _)
