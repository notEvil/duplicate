import config
import distributor
import log
import nestedtext
import pydantic
import argparse
import base64
import pathlib
import traceback


class _Configuration(pydantic.BaseModel):
    base: config.Configuration
    home_path: config.Path
    rofs_path: config.Path
    custodian_path: config.Path
    socket_path: config.Path
    authkey: str
    entry_db_path: config.Path


parser = argparse.ArgumentParser()
parser.add_argument("config")
arguments = parser.parse_args()

_ = nestedtext.load(pathlib.Path(arguments.config))
configuration = _Configuration.parse_obj(_)

log.start(level=configuration.base.log_level)

configuration.authkey = base64.b85decode(configuration.authkey.encode())

exit_functions = []
try:
    distributor.main(configuration, exit_functions)

except KeyboardInterrupt:
    for function in reversed(exit_functions):
        try:
            function()

        except:
            traceback.print_exc()
