import sap
import argparse
import pathlib
import struct
import sys


parser = argparse.ArgumentParser()

parser.add_argument("paths")

arguments = parser.parse_args()


base_paths = {}
path = pathlib.Path(arguments.paths)
if path.exists():
    with open(path) as file:
        for line in file:
            index = line.index(":")
            _ = int(line[:index].strip())
            base_paths[_] = pathlib.Path(line[index + 1 :].strip())


class _File:
    def __init__(self, path):
        super().__init__()

        self.path = path

        self._exists = path.exists()
        path.parent.mkdir(parents=True, exist_ok=True)
        self._file = open(path, "ab")

    def finalize(self):
        if self._file.closed:
            return

        size = self._file.tell()
        self._file.close()
        if size == 0 and not self._exists:
            self.path.unlink(missing_ok=True)

    def __del__(self):
        self.finalize()


stdin = sys.stdin.buffer
stdout = sys.stdout.buffer
files = []

try:
    while True:
        files.clear()

        (number,) = struct.unpack("<B", stdin.read(1))
        if number == 0:
            break

        error_code = None
        for _ in range(number):
            (key,) = struct.unpack("<B", stdin.read(1))
            (number,) = struct.unpack("<H", stdin.read(2))
            path_string = stdin.read(number).decode()
            if error_code is not None:
                continue

            base_path = base_paths.get(key)
            if base_path is None:
                error_code = sap._KEY_UNKNOWN
                files.clear()
                continue

            path = pathlib.Path(base_path, path_string)
            if not path.is_relative_to(base_path):
                error_code = sap._OUTSIDE
                files.clear()
                continue

            files.append(_File(path))

        if error_code is not None:
            stdout.write(struct.pack("<q", error_code))
            stdout.flush()
            break

        for file in files:
            stdout.write(struct.pack("<q", file._file.tell()))
        stdout.flush()

        for file in files:
            while True:
                (number,) = struct.unpack("<h", stdin.read(2))
                if number <= 0:
                    break
                file._file.write(stdin.read(number))

            file.finalize()
            if number < 0:
                break

        if number < 0:
            break

except:
    for file in files:
        file.finalize()
    raise
