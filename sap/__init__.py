import signal
import struct
import subprocess


_BUFFER_SIZE = 16 * 1024

_OUTSIDE = -1
_CANCELLED = -2
_KEY_UNKNOWN = -3

_ERROR_MESSAGES = {_OUTSIDE: "Outside of argument path", _KEY_UNKNOWN: "Key is unknown"}


def append(items, user_name, address, ssh_key_path, cancelled, min_size=10 * 1e6):
    items = iter(items)
    objects = []
    end = False

    _ = ["ssh", "-i", str(ssh_key_path), "{}@{}".format(user_name, address)]
    process = subprocess.Popen(_, stdin=subprocess.PIPE, stdout=subprocess.PIPE)
    stdin = process.stdin
    stdout = process.stdout

    try:
        while True:
            objects.clear()
            total_size = 0

            for key, (from_path, (to_key, to_path)) in items:
                try:
                    stat = from_path.stat()
                    file = open(from_path, "rb")

                except FileNotFoundError:
                    yield (key, False)
                    continue

                objects.append((key, file, to_key, to_path))
                total_size += stat.st_size

                if len(objects) == 100 or min_size <= total_size:
                    break

            stdin.write(struct.pack("<B", len(objects)))
            for _, _, to_key, to_path in objects:
                stdin.write(struct.pack("<B", to_key))
                bytes = str(to_path).encode()
                stdin.write(struct.pack("<H", len(bytes)))
                stdin.write(bytes)

            stdin.flush()
            if len(objects) == 0:
                break

            for _, file, _, _ in objects:
                (offset,) = struct.unpack("<q", stdout.read(8))

                error_message = _ERROR_MESSAGES.get(offset)
                if error_message is not None:
                    raise Exception(error_message)

                file.seek(offset)

            for key, file, _, _ in objects:
                while True:
                    if cancelled:
                        stdin.write(struct.pack("<h", _CANCELLED))
                        stdin.flush()
                        end = True
                        break

                    bytes = file.read(_BUFFER_SIZE)
                    stdin.write(struct.pack("<h", len(bytes)))
                    if len(bytes) == 0:
                        break

                    stdin.write(bytes)

                if end:
                    break

                file.close()
                yield (key, True)

            if end:
                break

        assert process.wait() == 0

    except:
        process.send_signal(signal.SIGINT)
        process.wait()
        raise

    finally:
        for _, file, _, _ in objects:
            file.close()
