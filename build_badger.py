from shell import *
import os.path as o_path
import pathlib

_NAME = "interface"

_PATH = pathlib.Path(__file__).parent

path = pathlib.Path(_PATH, "badger")
["go", "mod", "init", _NAME] >> shell(cwd=path) >> wait(expect=[0, 1])
["go", "get", "github.com/dgraph-io/badger/v3@v3.2103.2"] >> shell(cwd=path) >> wait

badger_path = pathlib.Path(_PATH, "python", "vendor", "badger")
if not badger_path.exists():
    (
        [
            "git",
            "clone",
            "-b",
            "v3.2103.2",
            "--depth",
            "1",
            "https://github.com/dgraph-io/badger.git",
            str(badger_path),
        ]
        >> shell
        >> wait
    )
    _ = pathlib.Path(_PATH, "badger.patch")
    ["patch", "-p1"] >> shell(cwd=badger_path, stdin=open(_)) >> wait

expected_line = "github.com/dgraph-io/badger/v3 v3.2103.2 => {}".format(
    o_path.relpath(badger_path, path)
)
with open(pathlib.Path(path, "go.mod"), "r+") as file:
    for line in file:
        if expected_line in line:
            break

    else:
        file.write("\nreplace (\n")
        file.write(expected_line)
        file.write("\n)")

(
    ["go", "build", "-buildmode", "c-shared", "-o", _NAME + ".so"]
    >> shell(cwd=path)
    >> wait
)
