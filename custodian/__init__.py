import badger
import builtin
import collector
import common
import magic
import rpyc
import rpyc.utils.authenticators as ru_authenticators
import datetime
import logging
import os
import pathlib
import pwd
import queue
import shutil
import ssl
import threading
import traceback


SOFT_FREE_SPACE = 0.1
HARD_FREE_SPACE = 0.09

_UPDATE_INTERVAL = 60


def main(configuration, exit_functions):
    home_path = pathlib.Path(configuration.base.root_path, configuration.home_path)
    duplicates_path = pathlib.Path(home_path, "d")
    duplicates_path.mkdir(exist_ok=True)

    _authorize_sap(duplicates_path, configuration)

    magic_ = magic.open(magic.MIME_TYPE)
    assert magic_.load() == 0
    decompress_magic = magic.open(magic.MIME_TYPE | magic.COMPRESS)
    assert decompress_magic.load() == 0
    magics = (magic_, decompress_magic)

    exit_event = threading.Event()

    _ = configuration.base
    custodians = common.Custodians(_.drives, _.ssl_key, _.ssl_certificate)
    exit_functions.append(custodians.close)

    with common.entry_db(configuration.home_path, configuration) as entry_db:
        _ = (
            entry_db,
            custodians,
            home_path,
            True,
            configuration.home_path,
            configuration,
            exit_event,
        )
        collector_thread = threading.Thread(target=collector.main, args=_)

        def _exit():
            exit_event.set()
            with custodians._condition:
                custodians._condition.notify_all()
            collector_thread.join()

        collector_thread.start()
        exit_functions.append(_exit)

        logging.info("Preparing")
        _remove_placeholder(entry_db, duplicates_path, configuration)

        transfers = []
        _ = (transfers, entry_db, magics, duplicates_path, configuration, exit_event)
        update_thread = threading.Thread(target=_update_transfers, args=_)

        def _exit():
            exit_event.set()
            update_thread.join()

        update_thread.start()
        exit_functions.append(_exit)

        _ = _Context(transfers, entry_db, duplicates_path, configuration)
        _process_requests(_, configuration, exit_functions)


def _authorize_sap(duplicates_path, configuration):
    home_path = pathlib.Path(pwd.getpwuid(os.getuid()).pw_dir)

    paths_path = pathlib.Path(home_path, "sap_paths")
    paths_path.touch()
    with open(paths_path, "r+") as file:
        expected_line = "{}: {}\n".format(configuration.base.drive_id, duplicates_path)
        for line in file:
            if line == expected_line:
                break
        else:
            file.write(expected_line)

    with open(configuration.base.ssh_public) as file:
        key_type, public_key, _ = file.read().split()

    authorized_keys_path = configuration.base.authorized_keys
    if authorized_keys_path is None:
        authorized_keys_path = pathlib.Path(home_path, ".ssh", "authorized_keys")

    authorized_keys_path.parent.mkdir(parents=True, exist_ok=True)
    authorized_keys_path.touch()
    with open(authorized_keys_path, "r+") as file:
        for line in file:
            if public_key in line:
                break
        else:
            _ = 'command="python -m sap {}" {} {} {}@localhost'
            _ = _.format(paths_path, key_type, public_key, configuration.base.user_name)
            file.write(_)


def _remove_placeholder(entry_db, duplicates_path, configuration):
    _ = pathlib.Path(duplicates_path.relative_to(configuration.base.root_path), " ")
    prefix = dict(path=str(_)[:-1])
    start_dictionary = None

    while True:
        try:
            with entry_db._path_dictionary.iterator(
                prefix=prefix, start=start_dictionary
            ) as iterator:
                iterator = iter(iterator)

                while True:
                    with entry_db.transaction(True) as t_entry_db:
                        for _, entry_dictionary in iterator:
                            if entry_dictionary["stat"].st_mode != 0:
                                continue

                            t_entry_db._files.remove(entry_dictionary)
                            if t_entry_db.is_full():
                                break
                        else:
                            break

                    start_dictionary = entry_dictionary

        except badger.ConflictError:
            continue

        break


def _update_transfers(
    transfers, entry_db, magics, duplicates_path, configuration, exit_event
):
    while True:
        logging.debug("Updating transfers")
        now = datetime.datetime.now()

        for index, transfer in builtin.enumerate_reversed(transfers):
            if transfer.update(entry_db, magics, duplicates_path, configuration):
                del transfers[index]

        logging.debug("Waiting for next update")
        _ = max(0, _UPDATE_INTERVAL - (datetime.datetime.now() - now).total_seconds())
        if exit_event.wait(_):
            break


class _Context:
    def __init__(self, transfers, entry_db, duplicates_path, configuration):
        super().__init__()

        self.transfers = transfers
        self.entry_db = entry_db
        self.duplicates_path = duplicates_path
        self.configuration = configuration


def _process_requests(context, configuration, exit_functions):
    queue_ = queue.Queue()

    logging.info("Starting server")

    authenticator = ru_authenticators.SSLAuthenticator(
        configuration.base.ssl_key,
        configuration.base.ssl_certificate,
        ca_certs=configuration.base.ssl_certificate,
        cert_reqs=ssl.CERT_REQUIRED,
    )
    _ = configuration.base.port
    server = rpyc.ThreadedServer(_Service(queue_), port=_, authenticator=authenticator)

    threading.Thread(target=server.start).start()
    exit_functions.append(server.close)

    while True:
        request = queue_.get()
        _PROCESSES[type(request)](request, context)


class _Service(rpyc.Service):
    def __init__(self, queue):
        super().__init__()

        self.queue = queue

    def exposed_request_duplication(self, argument, respond):
        self.queue.put(_DuplicationRequest(argument, respond))

    def exposed_request_transfer(self, argument, respond):
        self.queue.put(_TransferRequest(argument, respond))

    def exposed_request_exists(self, argument, respond):
        self.queue.put(_ExistsRequest(argument, respond))


class _Request:
    _EXCEPTION_TYPES = set()

    def __init__(self, argument, _respond):
        super().__init__()

        self.argument = argument
        self._respond = _respond

    def respond(self, argument):
        try:
            return self._respond(argument)

        except Exception as exception:  # TODO
            if type(exception) not in type(self)._EXCEPTION_TYPES:
                traceback.print_exc()
                type(self)._EXCEPTION_TYPES.add(type(exception))

        return None


_PROCESSES = {}


class _DuplicationRequest(_Request):
    pass


def _process_duplication_request(request, context):
    contents = request.argument
    logging.debug("Processing duplication request")

    plan = _Plan(contents, context)
    response = []
    counts = {}

    with context.entry_db.transaction(False) as t_entry_db:
        for _, state in plan.get_items(t_entry_db):
            response.append(state)
            counts[state] = counts.get(state, 0) + 1

    _ = "exist: {}, accepted: {}, rejected: {}, pushed: {:.2f}MB".format(
        counts.get(common._EXISTS, 0),
        counts.get(common._ACCEPT, 0),
        counts.get(common._REJECT, 0),
        plan._pushed_space / 1e6,
    )
    logging.debug(_)

    request.respond((tuple(response), context.configuration.base.drive_id))


_PROCESSES[_DuplicationRequest] = _process_duplication_request


class _TransferRequest(_Request):
    pass


_transfer_requests = {}


def _process_transfer_request(request, context):
    contents, drive_id = request.argument
    logging.debug("Processing transfer request")

    plan = _Plan(contents, context)
    items = []

    with context.entry_db.transaction(False) as t_entry_db:
        for item, state in plan.get_items(t_entry_db):
            if state != common._ACCEPT:
                logging.debug("rejected")
                request.respond(False)
                return

            items.append(item)

    while True:
        try:
            while True:
                with context.entry_db.transaction(True) as t_entry_db:
                    for index, (hash, (score, size, compressed)) in enumerate(items):
                        path = common.get_duplicate_path(hash)
                        if compressed:
                            path = path.with_suffix(".zst")

                        _ = pathlib.Path(context.duplicates_path, path)
                        entry_dictionary = dict(
                            path=_.relative_to(context.configuration.base.root_path),
                            stat=os.stat_result([0, 0, 0, 0, 0, 0, size, 0, 0, 0]),
                            hash=hash,
                            size=size,
                            _score=score,
                        )

                        t_entry_db._files.add(entry_dictionary)
                        t_entry_db._custodian_duplicates.add(hash, drive_id)

                        if t_entry_db.is_full():
                            break
                    else:
                        break

                items = items[index + 1 :]

        except badger.ConflictError:
            continue

        break

    if 0 < plan._abandoned_space:
        logging.debug("Deleting duplicates")

        _ = context.configuration.base.root_path
        duplicates_path = context.duplicates_path.relative_to(_)

        def _process(iterator, t_entry_db, abandoned_space):
            for score, entry_dictionary in iterator:
                if not entry_dictionary["path"].is_relative_to(duplicates_path):
                    continue

                # safety net
                if score <= plan._last_score:
                    if not (
                        score == plan._last_score
                        and plan._last_dictionary["_id"] <= entry_dictionary["_id"]
                    ):
                        return (0, None)

                for transfer in context.transfers:
                    if transfer._cancelled:
                        continue

                    if entry_dictionary["hash"] in transfer._hashes:
                        with transfer._lock:
                            transfer._cancelled = True
                            transfer._remaining_size = 0

                        transfer.transfer_request.respond(True)

                _ = context.configuration.base.root_path
                path = pathlib.Path(_, entry_dictionary["path"])
                path.unlink(missing_ok=True)

                t_entry_db._files.remove(entry_dictionary, _get=False)
                abandoned_space -= entry_dictionary["stat"].st_size

                if abandoned_space <= 0 or t_entry_db.is_full():
                    return (abandoned_space, entry_dictionary)

            return (0, None)

        # TODO races with collector, may lead to excessive deletes, see safety net
        abandoned_space = plan._abandoned_space
        start_dictionary = None
        while True:
            try:
                while True:
                    with context.entry_db.transaction(True) as t_entry_db:
                        with t_entry_db._score_dictionary.iterator(
                            reverse=True, prefix=True, start=start_dictionary
                        ) as iterator:
                            space, dictionary = _process(
                                iterator, t_entry_db, abandoned_space
                            )

                    if space <= 0:
                        break

                    abandoned_space = space
                    start_dictionary = dictionary

            except badger.ConflictError:
                continue

            break

    context.transfers.append(_Transfer(request))

    logging.debug("accepted")
    request.respond(True)


_PROCESSES[_TransferRequest] = _process_transfer_request


class _Plan:
    def __init__(self, contents, context):
        super().__init__()

        self.contents = contents
        self.context = context

        self._pushed_space = None
        self._abandoned_space = None
        self._last_dictionary = None

    def get_items(self, entry_db):
        root_path = self.context.configuration.base.root_path

        disk_usage = shutil.disk_usage(root_path)
        free_space = (
            disk_usage.free
            - sum(transfer._remaining_size for transfer in self.context.transfers)
            - SOFT_FREE_SPACE * disk_usage.total
        )
        pushed_space = 0

        mime_ranks = self.context.configuration.base.mime_ranks
        duplicates_path = self.context.duplicates_path.relative_to(root_path)
        reject = False
        duplicate_dictionary = None
        duplicate_score = None

        with entry_db._score_dictionary.iterator(reverse=True, prefix=True) as iterator:
            iterator = iter(iterator)

            for content in self.contents:
                hash, (score, size, _) = content

                if entry_db._hash_dictionary.contains(dict(hash=hash)):
                    yield (content, common._EXISTS)
                    continue

                if reject:
                    yield (content, common._REJECT)
                    continue

                while free_space < size:
                    try:
                        duplicate_score, duplicate_dictionary = next(iterator)

                    except StopIteration:
                        yield (content, common._REJECT)
                        reject = True
                        break

                    if not duplicate_dictionary["path"].is_relative_to(duplicates_path):
                        continue

                    if duplicate_score <= score:
                        yield (content, common._REJECT)
                        reject = True
                        break

                    _ = duplicate_dictionary["stat"].st_size
                    free_space += _
                    pushed_space += _

                if reject:
                    continue

                free_space -= size
                yield (content, common._ACCEPT)

        self._pushed_space = pushed_space
        _ = pushed_space - (SOFT_FREE_SPACE - HARD_FREE_SPACE) * disk_usage.total
        self._abandoned_space = _
        self._last_dictionary = duplicate_dictionary
        self._last_score = duplicate_score


class _Transfer:
    def __init__(self, transfer_request):
        super().__init__()

        self.transfer_request = transfer_request

        contents, _ = transfer_request.argument

        self._lock = threading.Lock()
        self._remaining_size = sum(size for _, (_, size, _) in contents)
        self._hashes = {hash for hash, _ in contents}
        self._cancelled = False

    def update(self, entry_db, magics, duplicates_path, configuration):
        contents, _ = self.transfer_request.argument
        end = (
            True
            if self._cancelled
            else (self.transfer_request.respond(None) is not False)
        )

        while True:
            iterator = iter(contents)
            remaining_size = 0

            try:
                while True:
                    with entry_db.transaction(end) as t_entry_db:
                        for hash, (score, size, compressed) in iterator:
                            _ = common.get_duplicate_path(hash)
                            path = pathlib.Path(duplicates_path, _)
                            if compressed:
                                path = path.with_suffix(".zst")

                            if end:
                                _ = path.relative_to(configuration.base.root_path)
                                entry_dictionary = t_entry_db.get(_)

                            try:
                                stat = path.lstat()

                            except FileNotFoundError:
                                if not end:  # transfer is alive
                                    continue
                                # transfer ended or is dead

                                if entry_dictionary is None:
                                    continue

                                t_entry_db._files.remove(entry_dictionary, _get=False)

                                if t_entry_db.is_full():
                                    break
                                continue

                            if not end:  # transfer is alive
                                remaining_size += max(0, size - stat.st_size)
                                continue
                            # transfer ended or is dead

                            if entry_dictionary is None:
                                continue

                            try:
                                common.update_metadata(path, stat, entry_dictionary)
                                common.update_content(
                                    path, stat, entry_dictionary, magics
                                )

                            except FileNotFoundError:
                                t_entry_db._files.remove(entry_dictionary, _get=False)

                            else:
                                entry_dictionary.pop("_score", None)
                                t_entry_db._files.add(entry_dictionary)

                            if t_entry_db.is_full():
                                break
                        else:
                            break

            except badger.ConflictError:
                continue

            break

        with self._lock:
            if not self._cancelled:
                self._remaining_size = remaining_size

        return end


class _ExistsRequest(_Request):
    pass


def _process_exists_request(request, context):
    hashes, is_custodian = request.argument
    home_path = context.configuration.home_path

    with context.entry_db.transaction(False) as t_entry_db:
        if is_custodian:

            def _exist(hash):
                _ = t_entry_db._hash_dictionary.get(dict(hash=hash))
                for entry_dictionary in _:
                    if entry_dictionary["path"].is_relative_to(home_path):
                        continue
                    return True
                return False

        else:
            _exist = lambda hash: t_entry_db._hash_dictionary.contains(dict(hash=hash))

        response = tuple(_exist(hash) for hash in hashes)

    request.respond(response)


_PROCESSES[_ExistsRequest] = _process_exists_request
