import badger
import common
import datetime
import logging
import pathlib
import pickle
import threading


def main(
    entry_db,
    custodians,
    home_path,
    is_custodian,
    custodian_path,
    configuration,
    exit_event,
):
    last_collect_path = pathlib.Path(home_path, "last_collect")

    if last_collect_path.exists():
        with open(last_collect_path, "rb") as file:
            last_collect, incompletes = pickle.load(file)
    else:
        last_collect = datetime.datetime.min

    while True:
        now = datetime.datetime.now()
        if common.UPDATE_INTERVAL <= (now - last_collect):
            logging.info("Collecting duplicates")
            incompletes = _remove_hashes(entry_db, is_custodian, custodian_path)

            last_collect = now
            with open(last_collect_path, "wb") as file:
                pickle.dump((last_collect, incompletes), file)

        while len(incompletes) != 0:
            logging.debug("Collecting duplicates for {}".format(repr(incompletes)))

            next_incompletes = set()
            for drive_id, hash_list in _get_hashes(incompletes, entry_db, is_custodian):
                if not _wait_for_custodians(
                    incompletes, last_collect, custodians, exit_event
                ):
                    break

                if not _update(
                    drive_id,
                    hash_list,
                    entry_db,
                    custodians,
                    is_custodian,
                    custodian_path,
                    configuration,
                ):
                    next_incompletes.add(drive_id)

            else:
                incompletes = next_incompletes
                with open(last_collect_path, "wb") as file:
                    pickle.dump((last_collect, incompletes), file)

                continue

            break

        logging.info("Waiting for next collect")
        _ = last_collect + common.UPDATE_INTERVAL - datetime.datetime.now()
        if exit_event.wait(max(0, _.total_seconds())):
            return


def _remove_hashes(entry_db, is_custodian, custodian_path):
    drive_ids = set()
    start_hash = None

    def _exist(hash, entry_db):
        return any(
            (not entry_dictionary["path"].is_relative_to(custodian_path)) ^ is_custodian
            for entry_dictionary in entry_db._hash_dictionary.get(dict(hash=hash))
        )  # distributor: any(not startswith), custodian: any(startswith)

    while True:
        try:
            with entry_db.transaction(False) as t_entry_db:
                _ = (
                    t_entry_db._custodian_duplicates
                    if is_custodian
                    else t_entry_db._distributor_duplicates
                )
                with _.iterator(start=start_hash) as iterator:
                    iterator = iter(iterator)

                    while True:
                        with entry_db.transaction(True) as t_entry_db:
                            duplicates = (
                                t_entry_db._custodian_duplicates
                                if is_custodian
                                else t_entry_db._distributor_duplicates
                            )
                            for hash, _drive_ids in iterator:
                                if _exist(hash, t_entry_db):
                                    drive_ids.update(_drive_ids)
                                    continue

                                duplicates.remove_all(hash)
                                if t_entry_db.is_full():
                                    break
                            else:
                                break

                        start_hash = hash

        except badger.ConflictError as exception:
            continue

        break

    return drive_ids


def _get_hashes(incompletes, entry_db, is_custodian):
    hash_lists = {}
    next_items = []
    start_hash = None

    while True:
        next_items.clear()

        _ = (
            entry_db._custodian_duplicates
            if is_custodian
            else entry_db._distributor_duplicates
        )
        with _.iterator(start=start_hash) as iterator:
            for hash, drive_ids in iterator:
                for drive_id in drive_ids:
                    if drive_id not in incompletes:
                        continue

                    hash_list = hash_lists.get(drive_id)
                    if hash_list is None:
                        hash_list = []
                        hash_lists[drive_id] = hash_list

                    hash_list.append(hash)
                    if len(hash_list) == common._BATCH_SIZE:
                        next_items.append((drive_id, hash_list))

                if len(next_items) != 0:
                    break
            else:
                break

        for item in next_items:
            yield item
            _, hash_list = item
            hash_list.clear()

        start_hash = hash

    for item in hash_lists.items():
        _, hash_list = item
        if len(hash_list) == 0:
            continue

        yield item


def _wait_for_custodians(incompletes, last_collect, custodians, exit_event):
    while True:
        if exit_event.is_set():
            return False

        waiting = False
        with custodians._condition:
            if any(drive_id in incompletes for drive_id, _ in custodians.get_items()):
                if waiting:
                    logging.debug("done")
                return True

            if not waiting:
                logging.debug("Waiting for custodians")
                waiting = True

            _ = last_collect + common.UPDATE_INTERVAL - datetime.datetime.now()
            if not custodians._condition.wait(max(0, _.total_seconds())):
                return False


def _update(
    drive_id,
    hash_list,
    entry_db,
    custodians,
    is_custodian,
    custodian_path,
    configuration,
):
    custodian = custodians.get(drive_id)
    if custodian is None:
        return False

    logging.debug("Requesting exists")

    event = threading.Event()
    exists = None

    def _respond(_exists):
        nonlocal exists
        exists = _exists
        event.set()

    if not custodian.request_exists((tuple(hash_list), is_custodian), _respond):
        logging.debug("failed")
        return False

    if not event.wait(common.TIMEOUT_SECONDS):
        logging.debug("timed out")
        return False

    while True:
        iterator = iter(zip(hash_list, exists))

        try:
            while True:
                with entry_db.transaction(True) as t_entry_db:
                    duplicates = (
                        t_entry_db._custodian_duplicates
                        if is_custodian
                        else t_entry_db._distributor_duplicates
                    )
                    for hash, exist in iterator:
                        if exist:
                            continue

                        last = duplicates.remove(hash, drive_id)
                        if not (last and is_custodian):
                            if t_entry_db.is_full():
                                break
                            continue

                        _ = t_entry_db._hash_dictionary.get(dict(hash=hash))
                        for entry_dictionary in _:
                            path = entry_dictionary["path"]
                            if not path.is_relative_to(custodian_path):
                                continue

                            path = pathlib.Path(configuration.base.root_path, path)
                            path.unlink(missing_ok=True)

                            t_entry_db._files.remove(entry_dictionary, _get=False)

                        if t_entry_db.is_full():
                            break
                    else:
                        break

        except badger.ConflictError:
            continue

        break

    logging.debug("done")
    return True
