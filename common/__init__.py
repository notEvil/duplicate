import builtin
import rofs
import setdb.rpyc as s_rpyc
import rpyc
import rpyc.core.protocol as rc_protocol
import zstandard
import base64
import contextlib
import datetime
import hashlib
import os
import pathlib
import queue
import re
import ssl
import stat
import struct
import threading
import time
import traceback


COMPRESSION_FACTOR = 0.9

UPDATE_INTERVAL = datetime.timedelta(days=1)
RETRY_SECONDS = 300
TIMEOUT_SECONDS = 60

_BATCH_SIZE = 5000  # TODO optimize
_TRANSACTION_SIZE = 0.99  # ~64MB * 0.15 * 0.01 ~= 96kB

_SIZE_BITS = 48  # 2**48 ~= 281TB
_RANK_ORIGIN = 2 ** (16 - 1)  # 16 + 48 = 64

_EXISTS = 1
_ACCEPT = 2
_REJECT = 3

_DECOMPRESS_MIME_TYPES = [
    "application/x-bzip2",
    "application/gzip",
    "application/x-lzma",
    "application/x-xz",
    "application/zstd",
]


# 244 * 4096 ~= 1MB, 10 * ~1MB ~= 10MB
def _get_bytes(file, size, buffer_size=4096, buffer_n=244, chunk_n=10):
    """
    - reads `chunk_n` evenly spread chunks from file
    - each chunk consists of `buffer_n` pieces
    - yields pieces of size `buffer_size`
    """
    chunk_size = buffer_n * buffer_size
    if size <= (chunk_n * chunk_size):
        while True:
            bytes = file.read(buffer_size)
            if len(bytes) == 0:
                break
            yield bytes

    else:
        step = (size - chunk_size) / (chunk_n - 1)
        position = 0

        for _ in range(chunk_n):
            file.seek(round(position))

            for _ in range(buffer_n):
                yield file.read(buffer_size)

            position += step


@contextlib.contextmanager
def entry_db(custodian_path, configuration):
    connection = s_rpyc.connect(configuration.socket_path, configuration.authkey)
    try:
        _ = connection.root.get_database(configuration.entry_db_path, create=True)
        with _ as database:
            yield _EntryDb(database, connection, custodian_path, configuration)
    finally:
        connection.close()


class _EntryDb:
    def __init__(self, database, connection, custodian_path, configuration):
        super().__init__()

        self.database = database
        self.connection = connection
        self.custodian_path = custodian_path
        self.configuration = configuration

        self._files = s_rpyc.Set(database.get_set("files", create=True), connection)

        get_object = connection.root.get_object

        _ = self._files.get_dictionary("path", get_object("path_key"), create=True)
        self._path_dictionary = s_rpyc.Dictionary(_, _path_key)
        _ = self._files.get_dictionary("hash", get_object("hash_key"), create=True)
        self._hash_dictionary = s_rpyc.Dictionary(_, _hash_key)

        if custodian_path is None:
            local_score_key = None
            remote_score_key = None

        else:
            _ = (str(custodian_path), tuple(configuration.base.mime_ranks.items()))
            local_score_key = _ScoreKey(*_)
            remote_score_key = get_object("score_key")(*_)

        _ = self._files.get_dictionary("score", remote_score_key, create=True)
        self._score_dictionary = s_rpyc.Dictionary(_, local_score_key)

        index_add = get_object("index_add")

        _ = database.get_set("distributor_duplicates", create=True, id_size=1)
        _ = _.get_dictionary("hash", None, create=True).get_index()
        self._distributor_duplicates = _Duplicates(_, index_add, self)

        _ = database.get_set("custodian_duplicates", create=True, id_size=1)
        _ = _.get_dictionary("hash", None, create=True).get_index()
        self._custodian_duplicates = _Duplicates(_, index_add, self)

        self._get_transaction_size = database.get_transaction_size

    @contextlib.contextmanager
    def transaction(self, write):
        with self.database.transaction(write) as database:
            yield _EntryDb(
                database, self.connection, self.custodian_path, self.configuration
            )

    def get(self, path):
        dictionaries = self._path_dictionary.get(dict(path=path))
        if len(dictionaries) == 0:
            return None
        (dictionary,) = dictionaries
        return dictionary

    def is_full(self):
        return _TRANSACTION_SIZE <= self._get_transaction_size()


def _path_key(dictionary):
    return get_key(dictionary["path"])


def _hash_key(dictionary):
    return dictionary.get("hash")


class _ScoreKey:
    def __init__(self, custodian_path, mime_ranks):
        super().__init__()

        self.custodian_path = custodian_path
        self.mime_ranks = mime_ranks

        self._custodian_path = pathlib.Path(custodian_path)
        self._mime_ranks = dict(mime_ranks)

    def __call__(self, object):
        if isinstance(object, bool):  # prefix
            return b"\1" if object else b"\0"

        if "hash" not in object:
            return None

        score = object.get("_score")
        if score is not None:
            return score

        _ = b"\1" if object["path"].is_relative_to(self._custodian_path) else b"\0"
        return _ + struct.pack(">Q", _get_score(object, self._mime_ranks))


def _get_score(dictionary, mime_ranks):
    size = dictionary["size"]
    uncompressed_size = dictionary["stat"].st_size
    if (uncompressed_size * COMPRESSION_FACTOR) < size:
        size = uncompressed_size

    return ((_RANK_ORIGIN - mime_ranks.get(dictionary["mime"], 0)) << _SIZE_BITS) | size


def _index_add(hash, drive_id, index):
    with index.set.database.transaction(True) as database:
        _ = database.get_set(index.set.name).get_dictionary(index.name, index.function)
        _.get_index().add(struct.pack("B", drive_id), hash)


class _Duplicates:
    def __init__(self, index, index_add, entry_db):
        super().__init__()

        self.index = index
        self.index_add = index_add
        self.entry_db = entry_db

    def add(self, hash, drive_id):
        self.index.add(struct.pack("B", drive_id), hash)

    def add_one(self, hash, drive_id):
        self.index_add(hash, drive_id, self.index)

    def contains(self, hash):
        return self.index.contains(hash)

    @contextlib.contextmanager
    def iterator(self, start=None):
        if start is not None:
            start = (b"\xff", start)

        def _(iterator):
            previous_hash = None
            drive_ids = []

            for bytes, hash in iterator:
                if hash == previous_hash:
                    drive_ids.append(bytes[0])
                    continue

                if len(drive_ids) != 0:
                    yield (previous_hash, drive_ids)

                previous_hash = hash
                drive_ids.clear()
                drive_ids.append(bytes[0])

            if len(drive_ids) != 0:
                yield (previous_hash, drive_ids)

        with self.index.iterator(start=start) as iterator:
            yield _(iterator)

    def remove(self, hash, drive_id):
        self.index.remove(struct.pack("B", drive_id), hash)
        return not self.index.contains(hash)

    def remove_all(self, hash):
        self.index.remove_all(hash)


def get_key(path):
    return builtin.encode(str(path).replace("/", "\0"))


def update_metadata(path, stat_, dictionary):
    dictionary["stat"] = stat_

    if stat.S_ISLNK(stat_.st_mode):
        dictionary["link"] = os.readlink(path)
    else:
        dictionary.pop("link", None)


def update_content(path, stat_, dictionary, magics):
    if not (stat.S_ISREG(stat_.st_mode) and stat_.st_size != 0):
        dictionary.pop("hash", None)
        dictionary.pop("size", None)
        dictionary.pop("mime", None)
        return

    hasher = hashlib.sha3_256()
    size = 0
    size_file = _SizeFile()
    compressor = zstandard.ZstdCompressor().stream_writer(size_file)

    with open(path, "rb") as file:
        for bytes in _get_bytes(file, stat_.st_size):
            hasher.update(bytes)
            size += len(bytes)
            compressor.write(bytes)

    compressor.flush(zstandard.FLUSH_FRAME)

    if size == 0:
        dictionary.pop("hash", None)
        dictionary.pop("size", None)
        dictionary.pop("mime", None)
        return

    dictionary["hash"] = hasher.digest()
    dictionary["size"] = round(stat_.st_size * (size_file._size / size))

    magic, decompress_magic = magics

    mime = magic.file(path)
    if _is_magic_file_not_found(mime):
        raise FileNotFoundError

    if mime in _DECOMPRESS_MIME_TYPES:
        mime = decompress_magic.file(path)
        if _is_magic_file_not_found(mime):
            raise FileNotFoundError

    dictionary["mime"] = mime


class _SizeFile:
    def __init__(self):
        super().__init__()

        self._size = 0

    def write(self, bytes):
        self._size += len(bytes)


def _is_magic_file_not_found(mime):
    if mime is None:
        return False
    _ = re.search(r"^cannot open .* \(No such file or directory\)$", mime)
    return _ is not None


class Custodians:
    def __init__(self, drives, key_path, certificate_path):
        super().__init__()

        self.drives = drives
        self.key_path = key_path
        self.certificate_path = certificate_path

        self._queue = queue.Queue()
        self._condition = threading.Condition()
        self._custodians = {}
        self._thread = threading.Thread(target=self._try_connect)
        self._closed = False

        for objects in drives.items():
            self._queue.put(objects)

        self._thread.start()

    def _try_connect(self):
        failed = []

        while True:
            retry_time = time.monotonic() + RETRY_SECONDS
            failed.clear()

            while True:
                try:
                    _ = max(0, retry_time - time.monotonic())
                    objects = self._queue.get(timeout=_)

                except queue.Empty:
                    break

                if self._closed:
                    return

                drive_id, (address, port) = objects

                try:
                    custodian = _Custodian(drive_id, address, port, self)

                except (OSError, ConnectionRefusedError):  # TODO
                    failed.append(objects)

                else:
                    with self._condition:
                        self._custodians[drive_id] = custodian
                        self._condition.notify_all()

                self._queue.task_done()

            for objects in failed:
                self._queue.put(objects)

    def get_items(self):
        with self._condition:
            return list(self._custodians.items())

    def get(self, drive_id):
        return self._custodians.get(drive_id)

    def close(self):
        self._closed = True
        self._queue.put(None)
        self._thread.join()

        for custodian in self._custodians.values():
            custodian.close()


class _Custodian:
    _EXCEPTION_TYPES = set()

    def __init__(self, drive_id, address, port, custodians):
        super().__init__()

        self.drive_id = drive_id
        self.address = address
        self.port = port
        self.custodians = custodians

        self._connection = rpyc.ssl_connect(
            address,
            port,
            keyfile=self.custodians.key_path,
            certfile=self.custodians.certificate_path,
            ca_certs=self.custodians.certificate_path,
            cert_reqs=ssl.CERT_REQUIRED,
        )
        root = self._connection.root
        self._request_duplication = root.request_duplication
        self._request_transfer = root.request_transfer
        self._request_exists = root.request_exists
        self._thread = threading.Thread(target=self._serve)

        self._thread.start()

    def _serve(self):
        try:
            while True:
                self._connection.serve(None)

        except (rc_protocol.socket.error, rc_protocol.select_error, IOError):
            if not self._connection.closed:
                raise

        except EOFError:
            pass

        finally:
            self.custodians._queue.put((self.drive_id, (self.address, self.port)))

    def request_duplication(self, argument, respond):
        try:
            self._request_duplication(argument, respond)
        except Exception as exception:
            self._except(exception)
            return False
        return True

    def request_transfer(self, argument, respond):
        try:
            self._request_transfer(argument, respond)
        except Exception as exception:
            self._except(exception)
            return False
        return True

    def request_exists(self, argument, respond):
        try:
            self._request_exists(argument, respond)
        except Exception as exception:
            self._except(exception)
            return False
        return True

    def _except(self, exception):  # TODO
        if type(exception) in type(self)._EXCEPTION_TYPES:
            return
        traceback.print_exc()
        type(self)._EXCEPTION_TYPES.add(type(exception))

    def close(self):
        self._connection.close()
        self._thread.join()


def get_duplicate_path(hash):
    string = base64.urlsafe_b64encode(hash).decode()
    # 64 / 64 / x = 488.28 at 2_000_000 files with 0.2% directories  # TODO reasonable?
    return pathlib.Path(string[:1], string[1:2], string[2:])
