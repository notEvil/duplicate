from shell import *
import ftplib
import pathlib
import sys

VERSION = "5.42"

_PATH = pathlib.Path(__file__).parent

name = "file-{}.tar.gz".format(VERSION)
path = pathlib.Path(_PATH, "python", "vendor", name)
path.parent.mkdir(parents=True, exist_ok=True)

with ftplib.FTP("ftp.astron.com") as ftp:
    ftp.login()
    with open(path, "wb") as file:
        ftp.retrbinary("RETR pub/file/" + name, file.write)

["tar", "-x", "-f", path.name] >> shell(cwd=path.parent) >> wait
path = pathlib.Path(path.parent, path.name[: -len(".tar.gz")])

[sys.executable, "-m", "build", "python"] >> shell(cwd=path) >> wait
