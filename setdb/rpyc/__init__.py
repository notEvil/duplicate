import badger
import setdb
import rpyc
import rpyc.utils.authenticators as ru_authenticators
import rpyc.utils.factory as ru_factory
import rpyc.utils.helpers as ru_helpers
import contextlib
import multiprocessing.connection as m_connection
import pickle
import threading


def start_server(socket_path, authkey, exposed_objects):
    assert not socket_path.exists()

    try:
        rpyc.ThreadedServer(
            _ServerService(exposed_objects),
            socket_path=str(socket_path),
            authenticator=lambda socket: _authenticate(socket, authkey, False),
            protocol_config=dict(allow_all_attrs=True),  # TODO restrict
        ).start()

    finally:
        socket_path.unlink(missing_ok=True)


class _ServerService(rpyc.Service):
    def __init__(self, exposed_objects):
        super().__init__()

        self.exposed_objects = exposed_objects

        self._databases = {}
        self._lock = threading.Lock()

    def on_connect(self, connection):
        connection._channel.compress = False

    def exposed_get_database(self, path, create=True):
        key = str(path)
        with self._lock:
            database = self._databases.get(key)
            if database is None:
                _ = badger.PlyvelDB(path, create_if_missing=create)
                database = setdb.Database(_)
                self._databases[key] = database

        return database

    def exposed_set_add(self, bytes, set):
        return set.add(pickle.loads(bytes), _bytes=bytes)

    def exposed_get_object(self, name):
        return self.exposed_objects.get(name)


def connect(socket_path, authkey):
    connection = ru_factory.unix_connect(
        str(socket_path),
        service=_ClientService(authkey),
        config=dict(  # TODO restrict
            allow_all_attrs=True,
            import_custom_exceptions=True,
            instantiate_custom_exceptions=True,
        ),
    )
    rpyc.BgServingThread(connection)
    return connection


class _ClientService(rpyc.Service):
    def __init__(self, authkey):
        super().__init__()

        self.authkey = authkey

    def on_connect(self, connection):
        socket = connection._channel.stream.sock
        _authenticate(socket, self.authkey, True)

        connection._channel.compress = False


def _authenticate(socket, authkey, is_client):
    connection = _Connection(socket)

    first_function = m_connection.deliver_challenge
    second_function = m_connection.answer_challenge
    if is_client:
        first_function, second_function = second_function, first_function

    try:
        first_function(connection, authkey)
        second_function(connection, authkey)

    except m_connection.AuthenticationError:
        raise ru_authenticators.AuthenticationError

    return (socket, None)


class _Connection:
    def __init__(self, socket):
        super().__init__()

        self.socket = socket

    def send_bytes(self, bytes):
        self.socket.send(bytes)

    def recv_bytes(self, size):
        return self.socket.recv(size)


class Set:
    def __init__(self, set, connection):
        super().__init__()

        self.set = set
        self.connection = connection

        self._add = connection.root.set_add
        self._get = set._plyvel_db.get

    def __getattr__(self, name):
        return getattr(self.set, name)

    def add(self, object):
        object["_id"] = self._add(pickle.dumps(object), self.set)

    def get(self, id):
        bytes = self._get(id)
        if bytes is None:
            return None

        object = pickle.loads(bytes)
        object["_id"] = id
        return object

    @contextlib.contextmanager
    def iterator(self):
        def _(iterator):
            for id, bytes in ru_helpers.buffiter(iterator):
                object = pickle.loads(bytes)
                object["_id"] = id
                yield object

        with self.set._plyvel_db.iterator() as iterator:
            yield _(iterator)


class Dictionary:
    def __init__(self, dictionary, function):
        super().__init__()

        self.dictionary = dictionary
        self.function = function

        self._get = dictionary._get
        self._contains = dictionary._contains

    def __getattr__(self, name):
        return getattr(self.dictionary, name)

    def get(self, object, iterator_arguments=None):
        if iterator_arguments is not None:
            iterator_arguments = tuple(iterator_arguments.items())

        objects = []
        _ = self._get(setdb._get_bytes(self.function(object)), iterator_arguments)
        for id, bytes in _:
            object = pickle.loads(bytes)
            object["_id"] = id
            objects.append(object)

        return objects

    def contains(self, object, iterator_arguments=None):
        if iterator_arguments is not None:
            iterator_arguments = tuple(iterator_arguments.items())

        return self._contains(setdb._get_bytes(object), iterator_arguments)

    @contextlib.contextmanager
    def iterator(
        self,
        reverse=False,
        prefix=None,
        start=None,
        start_inclusive=False,
        iterator_arguments=None,
    ):
        if prefix is not None:
            prefix = setdb._get_bytes(self.function(prefix))
        if start is not None:
            start = (start["_id"], setdb._get_bytes(self.function(start)))
        if iterator_arguments is not None:
            iterator_arguments = tuple(iterator_arguments.items())

        def _process(iterator):
            for id, value_bytes, bytes in ru_helpers.buffiter(iterator):
                object = pickle.loads(bytes)

                value = self.function(object)
                if value is None or setdb._get_bytes(value) != value_bytes:
                    continue  # workaround possible inconsistency

                object["_id"] = id
                yield (value, object)

        _ = self.dictionary._iterator(
            reverse, prefix, start, start_inclusive, iterator_arguments
        )
        with _ as iterator:
            yield _process(iterator)
