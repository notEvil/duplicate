import config
import log
import setdb.rpyc as s_rpyc
import nestedtext
import pydantic
import argparse
import base64
import importlib
import pathlib


class _Configuration(pydantic.BaseModel):
    socket_path: config.Path
    authkey: str
    exposed_objects: dict[str, str] = {}
    log_level: str = "INFO"


parser = argparse.ArgumentParser()
parser.add_argument("config")
arguments = parser.parse_args()

_ = nestedtext.load(pathlib.Path(arguments.config))
configuration = _Configuration.parse_obj(_)

log.start(level=configuration.log_level)

authkey = base64.b85decode(configuration.authkey.encode())

exposed_objects = {}
for name, path in configuration.exposed_objects.items():
    index = path.rindex(".")
    _ = importlib.import_module(path[:index])
    exposed_objects[name] = getattr(_, path[index + 1 :])

s_rpyc.start_server(configuration.socket_path, authkey, exposed_objects)
