import contextlib
import pickle
import struct
import threading
import weakref


_SEQUENCE_BANDWIDTH = 100
_ARRAY_SIZE = 10
_TRANSACTION_SIZE = 0.99


class Database:
    def __init__(self, plyvel_db, _meta=None):
        super().__init__()

        self.plyvel_db = plyvel_db
        self._meta = _Meta(self) if _meta is None else _meta

    def __enter__(self):
        return self

    def __exit__(self, type, value, traceback):
        self.close()

    @contextlib.contextmanager
    def transaction(self, write):
        if self.plyvel_db.in_transaction():
            assert not write
            yield self
            return

        with self.plyvel_db.transaction(write) as plyvel_db:
            yield Database(plyvel_db, _meta=self._meta)

    def get_transaction_size(self):
        return self.plyvel_db.get_transaction_size()

    def in_transaction(self):
        return self.plyvel_db.in_transaction()

    def is_transaction_open(self):
        return self.plyvel_db.is_transaction_open()

    def get_set_names(self):
        return self._meta.get_set_names()

    def get_set(self, name, create=False, id_size=5):
        return self._meta.get_set(name, create, id_size, self)

    def clean(self, iterator_arguments=None):
        count = 0

        for name in self.get_set_names():
            set = self.get_set(name)
            if set is None:
                continue

            count += set.clean(iterator_arguments=iterator_arguments)

        return count

    def close(self):
        if self.plyvel_db.in_transaction():
            return

        self._meta.close()

    def __del__(self):
        self.close()


class _Meta:
    def __init__(self, database):
        super().__init__()

        self.database = database

        self._lock = threading.RLock()
        self._plyvel_db = database.plyvel_db.prefixed_db(b"\0")
        self._prefix_sequence = self._plyvel_db.get_sequence(b"_prefix", 1)
        self._set_arguments = {}
        self._sets = {}
        self._index_arguments = {}
        self._indexes = weakref.WeakKeyDictionary()

    def get_set_names(self):
        _ = self._plyvel_db.prefixed_db(b"s").iterator(include_value=False)
        with _ as iterator:
            return [bytes.decode() for bytes in iterator]

    def get_set(self, name, create, id_size, database):
        with self._lock:
            arguments = self._set_arguments.get(name)

            if arguments is None:
                key = b"s" + name.encode()
                bytes = self._plyvel_db.get(key)
                if bytes is None:
                    if not create:
                        return None

                    arguments = dict(prefix=self._get_prefix(), id_size=id_size)
                    self._plyvel_db.put(key, pickle.dumps(arguments))

                else:
                    arguments = pickle.loads(bytes)

                arguments["sequence"] = self._plyvel_db.get_sequence(
                    b"a" + arguments["prefix"], _SEQUENCE_BANDWIDTH
                )

                self._set_arguments[name] = arguments

            set = _Set(name, database, **arguments)

            if database.plyvel_db.in_transaction():
                sets = self._sets.get(set.prefix)
                if sets is None:
                    sets = weakref.WeakSet()
                    self._sets[set.prefix] = sets

                sets.add(set)

        return set

    def _any_transaction_open(self, set):
        sets = self._sets.get(set.prefix)
        if sets is None:
            return False

        return any(set._plyvel_db.is_transaction_open() for set in sets)

    def remove_set(self, set):
        set.sequence.close()

        with self._plyvel_db.transaction(True) as t_plyvel_db:
            t_plyvel_db.delete(b"s" + set.name.encode())
            t_plyvel_db.delete(b"a" + set.prefix)

        del self._set_arguments[set.name]
        self._sets.pop(set.prefix, None)
        self._index_arguments.pop(set.prefix, None)

    def create_index(self, name, gap, set):
        _ = self._update_index(self._get_prefix(), gap, True, None, name, set)
        index = _Index(name, set, **_)

        self._indexes[set][name] = index
        return index

    def _get_prefix(self):
        return struct.pack("B", next(self._prefix_sequence) + 1)

    def get_indexes(self, set):
        indexes = self._indexes.get(set)
        if indexes is not None:
            return indexes

        with self._lock:
            arguments = self._index_arguments.get(set.prefix)

            if arguments is None:
                _ = self._plyvel_db.prefixed_db(b"i" + set.prefix).iterator()
                with _ as iterator:
                    arguments = {
                        name_bytes.decode(): pickle.loads(bytes)
                        for name_bytes, bytes in iterator
                    }

                self._index_arguments[set.prefix] = arguments

        indexes = {
            name: _Index(name, set, **arguments)
            for name, arguments in arguments.items()
        }

        self._indexes[set] = indexes
        return indexes

    def update_index(self, index):
        self._update_index(
            index.prefix,
            index.gap,
            index.incomplete,
            index.function,
            index.name,
            index.set,
        )

    def _update_index(self, prefix, gap, incomplete, function, name, set):
        index_arguments = self._index_arguments[set.prefix]

        arguments = index_arguments.get(name)
        if not (arguments is not None and arguments["incomplete"] == incomplete):
            arguments = dict(prefix=prefix, gap=gap, incomplete=incomplete)
            _ = b"i" + set.prefix + name.encode()
            self._plyvel_db.put(_, pickle.dumps(arguments))
            index_arguments[name] = arguments

        if function is not None:
            arguments["function"] = function

        return arguments

    def remove_index(self, index):
        self._plyvel_db.delete(b"i" + index.set.prefix + index.name.encode())
        del self._index_arguments[index.set.prefix][index.name]

    def close(self):
        for arguments in self._set_arguments.values():
            arguments["sequence"].close()

        self._prefix_sequence.close()


class _Set:
    def __init__(self, name, database, prefix, id_size, sequence):
        super().__init__()

        self.name = name
        self.database = database
        self.prefix = prefix
        self.id_size = id_size
        self.sequence = sequence

        self._lock = threading.Lock()
        self._plyvel_db = database.plyvel_db.prefixed_db(prefix)
        self._max_id = 2 ** (8 * id_size) - 1
        self._pack_format = ">" + " BHIIQQQQ"[id_size]

    def get_dictionary_names(self):
        with self._lock:
            return self.database._meta.get_indexes(self).keys()

    def get_dictionary(
        self, name, function, create=False, gap=b"\0", iterator_arguments=None
    ):
        with self._lock, self.database._meta._lock:
            index = self.database._meta.get_indexes(self).get(name)

            if index is None:
                if not create:
                    return None

                index = self.database._meta.create_index(name, gap, self)

            if index.function is None:
                index.function = function
                self.database._meta.update_index(index)

            if index.incomplete:
                index._rebuild(iterator_arguments)  # INFO might take some time

                index.incomplete = False
                self.database._meta.update_index(index)

        return _Dictionary(index)

    def add(self, object, _bytes=None):
        assert self._plyvel_db.in_transaction()

        with self._lock:
            id = object.get("_id")

            if id is not None:
                _bytes = None
                db_bytes = self._plyvel_db.get(id)

                if db_bytes is None:  # object was removed
                    del object["_id"]
                    id = None

            if id is None:
                id = next(self.sequence) + 1  # _Index occupies 0
                assert id <= self._max_id
                id = struct.pack(self._pack_format, id)[-self.id_size :]

                if _bytes is None:
                    _bytes = pickle.dumps(object)

                self._plyvel_db.put(id, _bytes)

                for index in self.database._meta.get_indexes(self).values():
                    value = index.function(object)
                    if value is None:
                        continue

                    index.add(id, _get_bytes(value))

                object["_id"] = id
                return id

            del object["_id"]
            try:
                self._plyvel_db.put(id, pickle.dumps(object))
            finally:
                object["_id"] = id

            db_object = pickle.loads(db_bytes)
            for index in self.database._meta.get_indexes(self).values():
                value = index.function(object)
                db_value = index.function(db_object)
                if value == db_value:
                    continue

                if db_value is not None:
                    index.remove(id, _get_bytes(db_value))

                if value is not None:
                    index.add(id, _get_bytes(value))

        return id

    def contains(self, id):
        return self._plyvel_db.get(id) is not None

    def get(self, id):
        bytes = self._plyvel_db.get(id)
        if bytes is None:
            return None

        object = pickle.loads(bytes)
        object["_id"] = id
        return object

    @contextlib.contextmanager
    def iterator(self, iterator_arguments=None):
        if iterator_arguments is None:
            iterator_arguments = {}

        def _(iterator):
            for id, bytes in iterator:
                object = pickle.loads(bytes)
                object["_id"] = id
                yield object

        with self._plyvel_db.iterator(**iterator_arguments) as iterator:
            yield _(iterator)

    def remove(self, object, _get=True):
        assert self._plyvel_db.in_transaction()

        with self._lock:
            id = object["_id"]

            if _get:
                bytes = self._plyvel_db.get(id)
                if bytes is None:
                    return

                object = pickle.loads(bytes)

            self._plyvel_db.delete(id)

            for index in self.database._meta.get_indexes(self).values():
                value = index.function(object)
                if value is None:
                    continue

                index.remove(id, _get_bytes(value))

    def clear(self, iterator_arguments=None):
        if iterator_arguments is None:
            iterator_arguments = {}

        assert not self._plyvel_db.in_transaction()

        with self.database._meta._lock:
            assert not self.database._meta._any_transaction_open(self)

            indexes = self.database._meta.get_indexes(self).values()

            for index in indexes:
                index.incomplete = True
                self.database._meta.update_index(index)

                index.clear(iterator_arguments=iterator_arguments)

            while True:
                with self._plyvel_db.transaction(True) as t_plyvel_db:
                    _ = iterator_arguments | dict(include_value=False)
                    with t_plyvel_db.iterator(**_) as iterator:
                        for id in iterator:
                            t_plyvel_db.delete(id)

                            if _TRANSACTION_SIZE <= t_plyvel_db.get_transaction_size():
                                break
                        else:
                            break

            for index in indexes:
                index.incomplete = False
                self.database._meta.update_index(index)

    def drop(self):
        assert not self._plyvel_db.in_transaction()

        with self.database._meta._lock:
            assert not self.database._meta._any_transaction_open(self)

            self.clear()

            for index in self.database._meta.get_indexes(self).values():
                self.database._meta.remove_index(index)

            self.database._meta.remove_set(self)

    def clean(self, iterator_arguments=None):
        with self._lock:
            return sum(
                index.clean(iterator_arguments=iterator_arguments)
                for index in self.database._meta.get_indexes(self).values()
            )


class _Index:
    def __init__(self, name, set, prefix, gap, incomplete, function=None):
        super().__init__()

        self.name = name
        self.set = set
        self.prefix = prefix
        self.gap = gap
        self.incomplete = incomplete
        self.function = function

        self._array_id = b"\0" * set.id_size
        self._max_length = 1 + _ARRAY_SIZE * set.id_size
        self._lock = threading.Lock()
        self._plyvel_db = set.database.plyvel_db.prefixed_db(prefix)

    def add(self, id, value_bytes):
        assert self._plyvel_db.in_transaction()

        with self._lock:
            array_key = self.gap.join((value_bytes, self._array_id))

            bytes = self._plyvel_db.get(array_key)
            if bytes is None:
                self._plyvel_db.put(array_key, b"\0" + id)
                return

            index, exist = self._get_index(id, bytes)
            if exist:
                return

            if bytes[0] == 0:
                if self._max_length <= len(bytes):
                    if index == len(bytes):
                        self._plyvel_db.put(array_key, b"\1" + bytes[1:])
                        self._plyvel_db.put(self.gap.join((value_bytes, id)), b"")

                    else:
                        _ = bytes[index : -self.set.id_size]
                        _ = b"".join((b"\1", bytes[1:index], id, _))
                        self._plyvel_db.put(array_key, _)
                        _ = self.gap.join((value_bytes, bytes[-self.set.id_size :]))
                        self._plyvel_db.put(_, b"")

                    return

                self._plyvel_db.put(array_key, id.join((bytes[:index], bytes[index:])))
                return

            if index == len(bytes):
                self._plyvel_db.put(self.gap.join((value_bytes, id)), b"")

            else:
                _ = id.join((bytes[:index], bytes[index : -self.set.id_size]))
                self._plyvel_db.put(array_key, _)
                _ = self.gap.join((value_bytes, bytes[-self.set.id_size :]))
                self._plyvel_db.put(_, b"")

    def contains(self, value_bytes, iterator_arguments=None):
        bytes = self._plyvel_db.get(self.gap.join((value_bytes, self._array_id)))
        if bytes is None:
            return False

        if len(bytes) != 1:
            return True

        id_size = self.set.id_size
        if iterator_arguments is None:
            iterator_arguments = {}

        _ = self._plyvel_db.prefixed_db(value_bytes + self.gap)
        with _.iterator(**(iterator_arguments | dict(include_value=False))) as iterator:
            return any(
                len(bytes) == id_size and bytes != self._array_id for bytes in iterator
            )

    def get(self, value_bytes, iterator_arguments=None):
        bytes = self._plyvel_db.get(self.gap.join((value_bytes, self._array_id)))
        if bytes is None:
            return

        id_size = self.set.id_size

        yield from (
            bytes[index : index + id_size] for index in range(1, len(bytes), id_size)
        )

        if bytes[0] == 0:
            return

        if iterator_arguments is None:
            iterator_arguments = {}

        _ = self._plyvel_db.prefixed_db(value_bytes + self.gap)
        with _.iterator(**(iterator_arguments | dict(include_value=False))) as iterator:
            yield from (
                bytes
                for bytes in iterator
                if len(bytes) == id_size and bytes != self._array_id
            )

    @contextlib.contextmanager
    def iterator(
        self,
        reverse=False,
        prefix=None,
        start=None,
        start_inclusive=False,
        iterator_arguments=None,
    ):
        id_size = self.set.id_size

        if start is None:
            _start = b""

        else:
            start_id, start_value_bytes = start

            _start = self.gap.join((start_value_bytes, self._array_id))
            bytes = self._plyvel_db.get(_start)
            if bytes is not None:
                index, _ = self._get_index(start_id, bytes)
                if index == len(bytes):
                    _start = self.gap.join((start_value_bytes, start_id))

            if prefix is not None:
                assert start_value_bytes.startswith(prefix)
                _start = _start[len(prefix) :]

        def _process(iterator):
            nonlocal start

            suffix_length = len(self.gap) + id_size

            for key_bytes, bytes in iterator:
                if prefix is not None:
                    if len(key_bytes) < suffix_length:
                        continue

                    key_bytes = prefix + key_bytes

                value_bytes = key_bytes[:-suffix_length]
                id = key_bytes[-id_size:]

                if id == self._array_id:
                    if start is not None:
                        start = None

                        if value_bytes == start_value_bytes:
                            index, exist = self._get_index(start_id, bytes)
                            if reverse:
                                if start_inclusive and exist:
                                    index += id_size
                                bytes = bytes[:index]

                            else:
                                if not start_inclusive and exist:
                                    index += id_size
                                # WARNING doesn't preserve first byte
                                bytes = bytes[index - 1 :]

                    indices = range(1, len(bytes), id_size)
                    if reverse:
                        indices = reversed(indices)

                    yield from (
                        (bytes[index : index + id_size], value_bytes)
                        for index in indices
                    )
                    continue

                if start is not None:
                    start = None

                    if (
                        not start_inclusive
                        and id == start_id
                        and value_bytes == start_value_bytes
                    ):
                        continue

                yield (id, value_bytes)

        if iterator_arguments is None:
            iterator_arguments = {}

        _ = self._plyvel_db if prefix is None else self._plyvel_db.prefixed_db(prefix)
        _ = _.iterator(**(iterator_arguments | dict(reverse=reverse, start=_start)))
        with _ as iterator:
            yield _process(iterator)

    def remove(self, id, value_bytes):
        assert self._plyvel_db.in_transaction()

        with self._lock:
            array_key = self.gap.join((value_bytes, self._array_id))

            bytes = self._plyvel_db.get(array_key)
            if bytes is None:
                return

            index, exist = self._get_index(id, bytes)

            if not exist:
                if index == len(bytes) and bytes[0] == 1:
                    self._plyvel_db.delete(self.gap.join((value_bytes, id)))
                return

            bytes = bytes[:index] + bytes[index + self.set.id_size :]
            if len(bytes) == 1:
                if bytes[0] == 0:
                    self._plyvel_db.delete(array_key)
                else:
                    self._plyvel_db.put(array_key, bytes)
                return

            self._plyvel_db.put(array_key, bytes)

    def _get_index(self, id, bytes):
        if len(bytes) == 1:
            return (1, False)

        id_size = self.set.id_size

        if bytes[-id_size:] < id:
            return (len(bytes), False)

        for index in range(1, len(bytes), id_size):
            index_id = bytes[index : index + id_size]
            if index_id < id:
                continue
            return (index, index_id == id)

    def remove_all(self, value_bytes, iterator_arguments=None):
        assert self._plyvel_db.in_transaction()

        with self._lock:
            array_key = self.gap.join((value_bytes, self._array_id))

            bytes = self._plyvel_db.get(array_key)
            if bytes is None:
                return

            self._plyvel_db.delete(array_key)
            if bytes[0] == 0:
                return

            id_size = self.set.id_size
            if iterator_arguments is None:
                iterator_arguments = {}

            prefix = value_bytes + self.gap
            _ = iterator_arguments | dict(include_value=False)
            with self._plyvel_db.prefixed_db(prefix).iterator(**_) as iterator:
                for bytes in iterator:
                    if len(bytes) != id_size:
                        continue

                    self._plyvel_db.delete(prefix + bytes)

    def _rebuild(self, iterator_arguments):
        self.clear()

        with self.set.iterator(iterator_arguments=iterator_arguments) as iterator:
            iterator = iter(iterator)

            while True:
                with self.set.database.transaction(True) as t_database:
                    t_index = _Index(
                        self.name,
                        t_database.get_set(self.set.name),
                        self.prefix,
                        self.gap,
                        True,
                        function=self.function,
                    )

                    for object in iterator:
                        value = self.function(object)
                        if value is None:
                            continue

                        t_index.add(object["_id"], _get_bytes(value))

                        _ = t_database.plyvel_db
                        if _TRANSACTION_SIZE <= _.get_transaction_size():
                            break
                    else:
                        break

    def clear(self, iterator_arguments=None):
        if iterator_arguments is None:
            iterator_arguments = {}

        with self._lock:
            while True:
                with self._plyvel_db.transaction(True) as t_plyvel_db:
                    _ = iterator_arguments | dict(include_value=False)
                    with t_plyvel_db.iterator(**_) as iterator:
                        for bytes in iterator:
                            t_plyvel_db.delete(bytes)

                            if _TRANSACTION_SIZE <= t_plyvel_db.get_transaction_size():
                                break
                        else:
                            break

    def clean(self, iterator_arguments=None):
        if iterator_arguments is None:
            iterator_arguments = {}

        assert not self._plyvel_db.in_transaction()

        with self.set.database._meta._lock:
            assert not self.set.database._meta._any_transaction_open(self.set)

            id_size = self.set.id_size
            previous_key_bytes = None
            any = True
            count = 0

            _ = iterator_arguments | dict(include_value=False)
            with self._plyvel_db.iterator(**_) as iterator:
                iterator = iter(iterator)

                while True:
                    with self._plyvel_db.transaction(True) as t_plyvel_db:
                        for key_bytes in iterator:
                            if key_bytes[-id_size:] != self._array_id:
                                any = True
                                continue

                            delete = not any
                            if delete:
                                t_plyvel_db.delete(previous_key_bytes)
                                count += 1

                            previous_key_bytes = key_bytes
                            any = False

                            _ = t_plyvel_db.get_transaction_size
                            if delete and _TRANSACTION_SIZE <= _():
                                break

                        else:
                            if not any:
                                t_plyvel_db.delete(previous_key_bytes)
                                count += 1

                            break

        return count


class _Dictionary:
    def __init__(self, index):
        super().__init__()

        self.index = index

    def get_index(self):
        return self.index

    def contains(self, object, iterator_arguments=None):
        _ = _get_bytes(self.index.function(object))
        return self.index.contains(_, iterator_arguments=iterator_arguments)

    def _contains(self, bytes, iterator_arguments):
        if isinstance(iterator_arguments, tuple):
            iterator_arguments = dict(iterator_arguments)
        return self.index.contains(bytes, iterator_arguments=iterator_arguments)

    def get(self, object, iterator_arguments=None):
        set = self.index.set
        with set._lock:
            _ = _get_bytes(self.index.function(object))
            _ = self.index.get(_, iterator_arguments=iterator_arguments)
            return [set.get(id) for id in _]

    def _get(self, bytes, iterator_arguments):
        if isinstance(iterator_arguments, tuple):
            iterator_arguments = dict(iterator_arguments)

        set = self.index.set
        with set._lock:
            _ = self.index.get(bytes, iterator_arguments=iterator_arguments)
            return tuple((id, set._plyvel_db.get(id)) for id in _)

    @contextlib.contextmanager
    def iterator(
        self,
        reverse=False,
        prefix=None,
        start=None,
        start_inclusive=False,
        iterator_arguments=None,
    ):
        """
        - doesn't lock
          - prevents deadlocks
          - skips inconsistencies (removed or modified objects)
        """
        if prefix is not None:
            prefix = _get_bytes(self.index.function(prefix))
        if start is not None:
            start = (start["_id"], _get_bytes(self.index.function(start)))

        def _(iterator):
            for id, value_bytes, bytes in iterator:
                object = pickle.loads(bytes)

                value = self.index.function(object)
                if value is None or _get_bytes(value) != value_bytes:
                    continue  # workaround possible inconsistency

                object["_id"] = id
                yield (value, object)

        _ = self._iterator(reverse, prefix, start, start_inclusive, iterator_arguments)
        with _ as iterator:
            yield _(iterator)

    @contextlib.contextmanager
    def _iterator(self, reverse, prefix, start, start_inclusive, iterator_arguments):
        if isinstance(iterator_arguments, tuple):
            iterator_arguments = dict(iterator_arguments)

        def _(iterator):
            set = self.index.set

            for id, value_bytes in iterator:
                bytes = set._plyvel_db.get(id)
                if bytes is None:  # workaround possible inconsistency
                    continue

                yield (id, value_bytes, bytes)

        with self.index.iterator(
            reverse=reverse,
            prefix=prefix,
            start=start,
            start_inclusive=start_inclusive,
            iterator_arguments=iterator_arguments,
        ) as iterator:
            yield _(iterator)

    def rebuild(self, iterator_arguments=None):
        set = self.index.set

        assert not self.index._plyvel_db.in_transaction()

        with set.database._meta._lock:
            assert not set.database._meta._any_transaction_open(set)

            self.index.incomplete = True
            set.database._meta.update_index(self.index)

            self.index._rebuild(iterator_arguments)

            self.index.incomplete = False
            set.database._meta.update_index(self.index)

    def drop(self, iterator_arguments=None):
        set = self.index.set

        assert not self.index._plyvel_db.in_transaction()

        with set.database._meta._lock:
            assert not set.database._meta._any_transaction_open(set)

            self.index.clear(iterator_arguments=iterator_arguments)

            set.database._meta.remove_index(self.index)

    def clean(self, iterator_arguments=None):
        return self.index.clean(iterator_arguments=iterator_arguments)


def _get_bytes(object):
    return object if isinstance(object, bytes) else pickle.dumps(object)
