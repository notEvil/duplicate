import config
from shell import *
import nestedtext
import argparse
import base64
import multiprocessing
import os
import pathlib
import pwd
import shutil
import stat
import sys
import time


_PATH = pathlib.Path(__file__).parent
_PERMISSION_MASK = stat.S_IRWXU | stat.S_IRWXG | stat.S_IRWXO


parser = argparse.ArgumentParser()
parser.add_argument("config")
arguments = parser.parse_args()

_ = nestedtext.load(pathlib.Path(arguments.config))
configuration = config.Configuration.parse_obj(_)


print("create user", file=sys.stderr)

process = ["useradd", "-m", configuration.user_name] >> shell >> wait(expect=[0, 9])

_ = pwd.getpwnam(configuration.user_name)
uid = _.pw_uid
user_path = pathlib.Path(_.pw_dir)


def _create_directory(*parts):
    path = pathlib.Path(*parts)
    if path.exists():
        _ = path.lstat()
        assert (_.st_mode & _PERMISSION_MASK) == stat.S_IRWXU and _.st_uid == uid
        return path

    path.mkdir(mode=stat.S_IRWXU)
    os.chown(path, uid, -1)
    return path


print("create directories", file=sys.stderr)

python_path = _create_directory(home_path, "python")
home_path = _create_directory(user_path, str(configuration.drive_id))
entry_db_path = _create_directory(home_path, "entry_db")
rofs_path = _create_directory(home_path, "rofs")
distributor_path = _create_directory(home_path, "distributor")

custodian_path = pathlib.Path(".duplicate")
_create_directory(configuration.root_path, custodian_path)


def _copy(from_path, to_path):
    if from_path.is_file():
        shutil.copy(from_path, to_path)
    else:
        shutil.copytree(from_path, to_path, dirs_exist_ok=True)
    _set_permissions(to_path)


def _set_permissions(path):
    if path.is_file():
        os.chmod(path, stat.S_IRUSR)
        os.chown(path, uid, -1)
        return

    os.chmod(path, stat.S_IRUSR | stat.S_IXUSR)
    os.chown(path, uid, -1)

    for path in path.iterdir():
        _set_permissions(path)


print("copy code and configuration", file=sys.stderr)

for parts in [
    ["badger"],
    ["builtin"],
    ["collector"],
    ["common"],
    ["config"],
    ["custodian"],
    ["distributor"],
    ["python", "vendor", "file-5.42", "python", "dist"],
    ["log"],
    ["python", "vendor", "pyfuse3", "dist"],
    ["rofs"],
    ["python", "vendor", "rpyc", "dist"],
    ["sap"],
    ["setdb"],
]:
    _ = pathlib.Path(python_path, *(parts[1:] if parts[0] == "python" else parts))
    _copy(pathlib.Path(_PATH, *parts), _)

_copy(pathlib.Path(_PATH, "sap"), pathlib.Path(user_path, "sap"))


_copy(pathlib.Path(_PATH, "python", "Pipfile"), pathlib.Path(python_path, "Pipfile"))
_ = pathlib.Path(_PATH, "python", "Pipfile.lock")
_copy(_, pathlib.Path(python_path, "Pipfile.lock"))
_create_directory(python_path, ".venv")

path = pathlib.Path(home_path, "certificate.key")
_copy(pathlib.Path(configuration.ssl_key), path)
configuration.ssl_key = path

path = pathlib.Path(home_path, "certificate.pem")
_copy(pathlib.Path(configuration.ssl_certificate), path)
configuration.ssl_certificate = path

path = pathlib.Path(home_path, "private.key")
_copy(pathlib.Path(configuration.ssh_private), path)
configuration.ssh_private = path

path = pathlib.Path(home_path, "public.key")
_copy(pathlib.Path(configuration.ssh_public), path)
configuration.ssh_public = path


print("prepare python", file=sys.stderr)

(
    ["pipenv", "sync"]
    >> shell(
        user=uid, env=dict(PATH="/usr/bin", LANG=os.environ["LANG"]), cwd=python_path
    )  # TODO improve
    >> wait
)


print("start setdb server", file=sys.stderr)

socket_path = pathlib.Path(home_path, "setdb.socket")
authkey = base64.b85encode(multiprocessing.current_process().authkey).decode()

configuration_path = pathlib.Path(home_path, "setdb_config")
setdb_configuration = dict(
    socket_path=socket_path,
    authkey=authkey,
    exposed_objects=dict(
        path_key="common._path_key",
        hash_key="common._hash_key",
        score_key="common._ScoreKey",
        index_add="common._index_add",
    ),
    log_level=configuration.log_level,
)
nestedtext.dump(setdb_configuration, configuration_path, default=str)
_set_permissions(configuration_path)

setdb_process = (
    [
        "pipenv",
        "run",
        "python",
        "-m",
        "setdb.rpyc",
        str(configuration_path),
    ]
    >> shell(user=uid, cwd=python_path, stdout=DEVNULL)
    >> background
)

time.sleep(1)  # TODO wait for setdb


print("mount readonly filesystem", file=sys.stderr)

configuration_path = pathlib.Path(home_path, "rofs_config")
rofs_configuration = dict(
    root_path=configuration.root_path,
    mount_path=rofs_path,
    uid=uid,
    log_level=configuration.log_level,
    socket_path=socket_path,
    authkey=authkey,
    entry_db_path=entry_db_path,
)
nestedtext.dump(rofs_configuration, configuration_path, default=str)
_set_permissions(configuration_path)

filesystem_process = (
    ["pipenv", "run", "python", "-m", "rofs", str(configuration_path)]
    >> shell(cwd=python_path, stdout=DEVNULL)
    >> background
)

time.sleep(1)  # TODO wait for rofs


print("start distributor", file=sys.stderr)

configuration_path = pathlib.Path(home_path, "distributor_config")
distributor_configuration = dict(
    base=configuration.dict(),
    home_path=distributor_path,
    rofs_path=rofs_path,
    custodian_path=custodian_path,
    socket_path=socket_path,
    authkey=authkey,
    entry_db_path=entry_db_path,
)
nestedtext.dump(distributor_configuration, configuration_path, default=str)
_set_permissions(configuration_path)

distributor_process = (
    [
        "pipenv",
        "run",
        "python",
        "-m",
        "distributor",
        str(configuration_path),
    ]
    >> shell(user=uid, cwd=python_path, stdout=DEVNULL)
    >> background
)


print("start custodian", file=sys.stderr)

configuration_path = pathlib.Path(home_path, "custodian_config")
custodian_configuration = dict(
    base=configuration.dict(),
    home_path=custodian_path,
    socket_path=socket_path,
    authkey=authkey,
    entry_db_path=entry_db_path,
)
nestedtext.dump(custodian_configuration, configuration_path, default=str)
_set_permissions(configuration_path)

custodian_process = (
    [
        "pipenv",
        "run",
        "python",
        "-m",
        "custodian",
        str(configuration_path),
    ]
    >> shell(user=uid, cwd=python_path, stdout=DEVNULL)
    >> background
)


input("Press Enter to exit")


custodian_process >> interrupt >> wait(expect=None)  # stop custodian
distributor_process >> interrupt >> wait(expect=None)  # stop distributor
filesystem_process >> interrupt >> wait(expect=None)  # unmount readonly filesystem
setdb_process >> interrupt >> wait(expect=None)  # stop setdb server
