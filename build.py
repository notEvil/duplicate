from shell import *
import sys

[sys.executable, "./build_badger.py"] >> shell >> wait
[sys.executable, "./build_magic.py"] >> shell >> wait
[sys.executable, "./build_pyfuse3.py"] >> shell >> wait
[sys.executable, "./build_rpyc.py"] >> shell >> wait
