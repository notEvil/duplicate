import logging
import os
import threading


def start(level=logging.INFO, limit_peewee=True):
    handler = logging.StreamHandler()
    handler.setFormatter(_Formatter())
    logging.basicConfig(level=level, handlers=[handler])

    if limit_peewee and (level == "DEBUG" or level == logging.DEBUG):
        logging.getLogger("peewee").setLevel(logging.INFO)


class _Formatter(logging.Formatter):
    def __init__(self):
        super().__init__()

        self._process_code = self._code(os.getpid(), upper=True)
        self._thread_codes = {}
        self._lock = threading.Lock()

    def format(self, record):
        message = record.msg
        if len(record.args) != 0:
            message = message % record.args

        return "{}|{:8}|{}{}|{} ({}, {}, {})".format(
            self.formatTime(record),
            record.levelname,
            self._process_code,
            self._get_thread_code(record.thread),
            message,
            record.name,
            record.threadName,
            record.process,
        )

    def _get_thread_code(self, thread_id):
        code = self._thread_codes.get(thread_id)
        if code is not None:
            return code

        with self._lock:
            code = self._code(len(self._thread_codes) + 1)
            self._thread_codes[thread_id] = code

        return code

    def _code(self, number, upper=False):
        characters = []
        for _ in range(2):
            digit = number % 27
            _ = " " if digit == 0 else chr(ord("A" if upper else "a") + digit - 1)
            characters.append(_)
            number //= 27

        return "".join(reversed(characters))
