from shell import *
import pathlib
import sys

_PATH = pathlib.Path(__file__).parent

path = pathlib.Path(_PATH, "python", "vendor", "pyfuse3")
path.parent.mkdir(parents=True, exist_ok=True)
if not path.exists():
    (
        [
            "git",
            "clone",
            "--depth",
            "1",
            "https://github.com/libfuse/pyfuse3",
            str(path),
        ]
        >> shell
        >> wait
    )
[sys.executable, "./setup.py", "build_cython"] >> shell(cwd=path) >> wait
[sys.executable, "-m", "build"] >> shell(cwd=path) >> wait
