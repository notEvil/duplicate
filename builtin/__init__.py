def enumerate_reversed(sequence):
    return zip(reversed(range(len(sequence))), reversed(sequence))


def zip_sorted(
    objects,
):  # (objects~(iterable~iter(object), get key~function(object): key~object))
    objects = [(iter(iterable), get_key) for iterable, get_key in objects]
    list = []

    for index, (iterator, get_key) in enumerate(objects):
        object = next(iterator, None)
        if object is None:
            continue
        list.append((get_key(object), index, object))

    if len(list) == 0:
        return

    nones = [None] * len(objects)
    while len(list) != 0:
        list.sort(reverse=True)

        next_result = nones.copy()
        next_key = list[-1][0]

        for index, (key, object_index, object) in enumerate_reversed(list):
            if key != next_key:
                break

            next_result[object_index] = object

            iterator, get_key = objects[object_index]
            object = next(iterator, None)
            if object is None:
                del list[index]

            else:
                list[index] = (get_key(object), object_index, object)

        yield next_result


def encode(string):
    return string.encode(errors="surrogateescape")


def decode(bytes):
    return bytes.decode(errors="surrogateescape")
