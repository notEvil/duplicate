import pyfuse3
import os


class IteratingOperations:
    async def readdir(self, inode, start_id, token):
        """
        - yields (name~str, attributes~pyfuse3.EntryAttributes)
        - yield returns result of pyfuse3.readdir_reply
        """
        raise NotImplementedError


class FromIteratingOperations:
    def __init__(self, iterating_operations):
        super().__init__()

        self.iterating_operations = iterating_operations

    def __getattr__(self, name):
        return getattr(self.iterating_operations, name)

    async def readdir(self, inode, start_id, token):
        items = self.iterating_operations.readdir(inode, start_id, token)
        result = None

        while True:
            try:
                name, attributes = await items.asend(result)
            except StopAsyncIteration:
                break

            result = pyfuse3.readdir_reply(
                token, os.fsencode(name), attributes, attributes.st_ino
            )


class CollectedOperations(IteratingOperations):
    async def _forget_inode(self, inode):
        raise NotImplementedError


class FromCollectedOperations:
    """
    - not implemented
      - create
      - link
      - mkdir
      - mknod
      - rename
      - rmdir
      - symlink
      - unlink
    """

    def __init__(self, collected_operations):
        super().__init__()

        self.collected_operations = collected_operations

        self._lookup_counts = {}

    def __getattr__(self, name):
        return getattr(self.collected_operations, name)

    async def lookup(self, parent_inode, name, context):
        attributes = await self.collected_operations.lookup(parent_inode, name, context)
        self._count(attributes.st_ino)
        return attributes

    async def readdir(self, file_handle, start_id, token):
        items = self.collected_operations.readdir(file_handle, start_id, token)
        result = None

        while True:
            try:
                item = await items.asend(result)
            except StopAsyncIteration:
                break

            result = yield item
            if result:
                _, attributes = item
                self._count(attributes.st_ino)

    def _count(self, inode):
        self._lookup_counts[inode] = self._lookup_counts.get(inode, 0) + 1

    async def forget(self, items):
        for inode, n in items:
            count = self._lookup_counts[inode]
            if count == n:
                del self._lookup_counts[inode]
                await self.collected_operations._forget_inode(inode)

            else:
                self._lookup_counts[inode] = count - n


class SeeklessOperations(IteratingOperations):
    def _add_entry(self, name, attributes, file_handle):
        raise NotImplementedError


class FromSeeklessOperations:
    def __init__(self, seekless_operations):
        super().__init__()

        self.seekless_operations = seekless_operations

        self._objects = {}

    def __getattr__(self, name):
        return getattr(self.seekless_operations, name)

    async def readdir(self, file_handle, start_id, token):
        if start_id == 0:
            previous_id = None

        else:
            previous_id, item = self._objects[file_handle]
            assert start_id == previous_id

            if item is None:  # end
                return

            result = yield item
            if not result:
                return

            name, attributes = item
            self.seekless_operations._add_entry(name, attributes, file_handle)

            previous_id = attributes.st_ino

        items = self.seekless_operations.readdir(file_handle, start_id, token)
        result = None

        while True:
            try:
                item = await items.asend(result)

            except StopAsyncIteration:
                if result is not False:
                    self._objects[file_handle] = (previous_id, None)  # end
                break

            result = yield item
            if not result:
                self._objects[file_handle] = (previous_id, item)

            _, attributes = item
            previous_id = attributes.st_ino

    async def releasedir(self, file_handle):
        self._objects.pop(file_handle, None)
        await self.seekless_operations.releasedir(file_handle)
