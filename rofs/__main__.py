import config
import log
import rofs
import rofs.fuse as r_fuse
import nestedtext
import pydantic
import pyfuse3
import trio
import argparse
import os
import pathlib


class _Configuration(pydantic.BaseModel):
    root_path: config.Path
    mount_path: config.Path
    uid: int
    log_level: str = "INFO"


parser = argparse.ArgumentParser()
parser.add_argument("config")
arguments = parser.parse_args()

_ = nestedtext.load(pathlib.Path(arguments.config))
configuration = _Configuration.parse_obj(_)

log.start(level=configuration.log_level)


_ = rofs._Operations(configuration.root_path, configuration.uid, os.getgid())
_ = r_fuse.FromCollectedOperations(r_fuse.FromSeeklessOperations(_))
pyfuse3.init(
    r_fuse.FromIteratingOperations(_),
    str(configuration.mount_path),
    pyfuse3.default_options | {"ro", "allow_other"},
)

try:
    trio.run(pyfuse3.main)

except KeyboardInterrupt:
    pass

finally:
    pyfuse3.close()
