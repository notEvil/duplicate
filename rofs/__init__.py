import rofs.fuse as r_fuse
import pyfuse3
import trio
import zstandard
import bz2
import errno
import gzip
import io
import itertools
import lzma
import os
import pickle
import stat
import zlib


_MODE_MASK = ~(stat.S_IRWXU | stat.S_IRWXG | stat.S_IRWXO)
_FILE_PERMISSIONS = stat.S_IRUSR
_DIRECTORY_PERMISSIONS = stat.S_IRUSR | stat.S_IXUSR

_EXTENSION_SHIFT = 64 - 3
_BASE_ID = 1
_STAT_ID = 2
_DECOMPRESS_BZIP2_ID = 3
_DECOMPRESS_GZIP_ID = 4
_DECOMPRESS_LZMA_ID = 5
_DECOMPRESS_ZSTD_ID = 6
_COMPRESS_ZSTD_ID = 7

_BASE_INODE = _BASE_ID << _EXTENSION_SHIFT
_BASE_MASK = (1 << _EXTENSION_SHIFT) - 1

_STAT_SIZE = int(1e6)

_DECOMPRESSORS = {
    _DECOMPRESS_BZIP2_ID: bz2.open,
    _DECOMPRESS_GZIP_ID: gzip.open,
    _DECOMPRESS_LZMA_ID: lzma.open,
    _DECOMPRESS_ZSTD_ID: lambda file: zstandard.ZstdDecompressor().stream_reader(file),
}
_DECOMPRESS_SIZE = 2**48

_COMPRESSORS = {
    _COMPRESS_ZSTD_ID: lambda file: zstandard.ZstdCompressor().stream_reader(file)
}


class _Operations(
    r_fuse.CollectedOperations, r_fuse.SeeklessOperations, pyfuse3.Operations
):
    _EXTENSION_IDS = {
        "b": _BASE_ID,
        "stat": _STAT_ID,
        "decompress_bzip2": _DECOMPRESS_BZIP2_ID,
        "decompress_gzip": _DECOMPRESS_GZIP_ID,
        "decompress_lzma": _DECOMPRESS_LZMA_ID,
        "decompress_zstd": _DECOMPRESS_ZSTD_ID,
        "compress_zstd": _COMPRESS_ZSTD_ID,
    }

    _SIZES = {_STAT_ID: _STAT_SIZE}
    _SIZES.update((id, _DECOMPRESS_SIZE) for id in _DECOMPRESSORS)

    def __init__(self, path, uid, gid):
        super().__init__()

        self.path = path
        self.uid = uid
        self.gid = gid

        self._device_id = path.stat().st_dev
        self._paths = {_BASE_INODE: trio.Path(path)}  # {inode~int: trio.Path}

        self._file_handles = iter(itertools.count())

        self._inodes = {}  # {file handle~int: inode~int}
        self._iterators = {}  # {file handle~int: iterator~iter(path~trio.Path)}

        self._files = {}  # {file handle~int: file}

    async def lookup(self, parent_inode, name, context):
        name = os.fsdecode(name)

        attributes = None
        extension_id = parent_inode >> _EXTENSION_SHIFT

        if extension_id == _BASE_ID:
            path = trio.Path(self._paths[parent_inode], name)
            attributes = await self._get_attributes(path)
            self._paths[attributes.st_ino] = path

        elif (
            extension_id == _STAT_ID
            or extension_id in _DECOMPRESSORS
            or extension_id in _COMPRESSORS
        ):
            index = name.index("_")
            parent_inode = int(name[:index])
            name = name[index + 1 :]

            path = trio.Path(self._paths[parent_inode], name)  # TODO may fail
            stat_ = await self._get_stat(path)

            attributes = pyfuse3.EntryAttributes()
            attributes.st_ino = stat_.st_ino | (extension_id << _EXTENSION_SHIFT)
            attributes.st_mode = stat.S_IFREG | _FILE_PERMISSIONS
            attributes.st_size = type(self)._SIZES.get(extension_id, stat_.st_size)

            self._paths[attributes.st_ino] = path

        elif extension_id == 0:
            if parent_inode == pyfuse3.ROOT_INODE:
                extension_id = type(self)._EXTENSION_IDS.get(name)
                if extension_id is not None:
                    _ = extension_id << _EXTENSION_SHIFT
                    attributes = await self.getattr(_, None)

        if attributes is None:
            raise pyfuse3.FUSEError(errno.ENOENT)

        attributes.st_uid = self.uid
        attributes.st_gid = self.gid
        return attributes

    async def opendir(self, inode, context):
        file_handle = next(self._file_handles)
        self._inodes[file_handle] = inode
        return file_handle

    async def readdir(self, file_handle, start_id, token):
        inode = self._inodes[file_handle]
        if (inode >> _EXTENSION_SHIFT) != _BASE_ID:
            return

        # INFO pathlib.Path.iterdir and trio.Path.iterdir don't use os.scandir
        iterator = (
            (await self._paths[inode].iterdir())
            if start_id == 0
            else self._iterators[file_handle]
        )
        for path in iterator:
            try:
                attributes = await self._get_attributes(path)

            except pyfuse3.FUSEError as fuse_error:
                if fuse_error.errno == errno.ENOENT:
                    continue

                raise fuse_error

            result = yield (path.name, attributes)
            if not result:
                break

            self._paths[attributes.st_ino] = path

        self._iterators[file_handle] = iterator

    def _add_entry(self, name, attributes, file_handle):
        _ = trio.Path(self._paths[self._inodes[file_handle]], name)
        self._paths[attributes.st_ino] = _

    async def releasedir(self, file_handle):
        self._iterators.pop(file_handle, None)
        del self._inodes[file_handle]

    async def getattr(self, inode, context):
        attributes = None
        extension_id = inode >> _EXTENSION_SHIFT

        if extension_id == _BASE_ID and (inode & _BASE_MASK) != 0:
            attributes = await self._get_attributes(self._paths[inode])

        elif (
            extension_id == _STAT_ID
            or extension_id in _DECOMPRESSORS
            or extension_id in _COMPRESSORS
        ) and (inode & _BASE_MASK) != 0:
            size = type(self)._SIZES.get(extension_id)
            if size is None:
                size = (await self._get_stat(self._paths[inode])).st_size

            attributes = pyfuse3.EntryAttributes()
            attributes.st_ino = inode
            attributes.st_mode = stat.S_IFREG | _FILE_PERMISSIONS
            attributes.st_size = size

        elif (extension_id == 0 and inode == pyfuse3.ROOT_INODE) or (
            extension_id != 0 and (inode & _BASE_MASK) == 0
        ):
            attributes = pyfuse3.EntryAttributes()
            attributes.st_ino = inode
            attributes.st_mode = stat.S_IFDIR | _DIRECTORY_PERMISSIONS

        if attributes is None:
            raise pyfuse3.FUSEError(errno.ENOENT)

        attributes.st_uid = self.uid
        attributes.st_gid = self.gid
        return attributes

    async def _get_attributes(self, path):
        stat_ = await self._get_stat(path)

        attributes = pyfuse3.EntryAttributes()
        attributes.st_ino = stat_.st_ino | _BASE_INODE
        _ = _DIRECTORY_PERMISSIONS if stat.S_ISDIR(stat_.st_mode) else _FILE_PERMISSIONS
        attributes.st_mode = (stat_.st_mode & _MODE_MASK) | _
        attributes.st_uid = self.uid
        attributes.st_gid = self.gid
        attributes.st_ctime_ns = stat_.st_ctime_ns
        attributes.st_mtime_ns = stat_.st_mtime_ns
        attributes.st_size = stat_.st_size
        return attributes

    async def readlink(self, inode, context):
        path = self._paths[inode]

        try:
            path = await path.readlink()
        except OSError as os_error:
            raise pyfuse3.FUSEError(os_error.errno)

        return os.fsencode(path)

    async def open(self, inode, flags, context):
        extension_id = inode >> _EXTENSION_SHIFT
        if extension_id == _BASE_ID:
            try:
                file = open(self._paths[inode], "rb")
            except OSError as os_error:
                raise pyfuse3.FUSEError(os_error.errno)

        elif extension_id == _STAT_ID:
            file = io.BytesIO(pickle.dumps(await self._get_stat(self._paths[inode])))

        elif extension_id in _DECOMPRESSORS:
            try:
                file = open(self._paths[inode], "rb")
            except OSError as os_error:
                raise pyfuse3.FUSEError(os_error.errno)

            file = _DECOMPRESSORS[extension_id](file)

        elif extension_id in _COMPRESSORS:
            try:
                file = open(self._paths[inode], "rb")
            except OSError as os_error:
                raise pyfuse3.FUSEError(os_error.errno)

            file = _COMPRESSORS[extension_id](file)

        else:
            raise Exception

        file_handle = next(self._file_handles)
        self._files[file_handle] = file
        return pyfuse3.FileInfo(fh=file_handle)

    async def _get_stat(self, path):
        try:
            stat = await path.lstat()
        except OSError as os_error:
            raise pyfuse3.FUSEError(os_error.errno)

        if stat.st_dev != self._device_id:
            raise pyfuse3.FUSEError(errno.ENOENT)

        return stat

    async def read(self, file_handle, offset, size):
        file = self._files[file_handle]
        try:
            if file.tell() != offset:
                file.seek(offset)
            return file.read(size)

        except (
            gzip.BadGzipFile,
            EOFError,
            zlib.error,
            lzma.LZMAError,
            zstandard.ZstdError,
        ):
            raise pyfuse3.FUSEError(errno.EIO)

        except OSError as os_error:
            if os_error.errno is None:
                raise pyfuse3.FUSEError(errno.EIO)

            raise pyfuse3.FUSEError(os_error.errno)

    async def release(self, file_handle):
        self._files.pop(file_handle).close()

    async def _forget_inode(self, inode):
        if inode == _BASE_INODE:
            return

        self._paths.pop(inode, None)


def get_shortcut_name(path):
    return "_".join((repr(path.parent.lstat().st_ino), path.name))
